package it.polito.ccn.simulator;

import java.security.MessageDigest;
import java.util.Iterator;

import org.junit.Assert;

import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.generated.UriG;

public class Uri 
{	
	private static MessageDigest digestFunction = null;
	private static boolean hashEnabled;
	
	private String[] components;
	private String uriRepresentation;
	/* Represents the URI size in byte (one byte for each character, excluding "/") */
	private int uriSize = 0;
	private String id;
	private String[] array;
	private int[][] hashes;
	private int[] res = new int[BloomFilter.HASHES];
	
	static
	{
		try
		{
			if(CCNNetwork.pitType.equals("DiPIT") || CCNNetwork.pitType.equals("DiPITBF"))
				hashEnabled = true;
			else
				hashEnabled = false;
			digestFunction = java.security.MessageDigest.getInstance("SHA-1");
		} catch(Exception ex) { ex.printStackTrace(); }
	}
	
	public Uri(UriG uriG) throws BadUriException
	{
		if(uriG.getComponent().size() < 1)
			throw new BadUriException("Too short URI!");
		components = new String[uriG.getComponent().size()];
		Iterator<String> it = uriG.getComponent().iterator();
		int i = 0;
		while(it.hasNext())
			components[i++] = it.next().toString();
		uriRepresentation = "";
		for(i = 0; i < components.length; i++)
		{
			uriSize+=components[i].length();
			uriRepresentation = uriRepresentation.concat("/" + components[i]);
		}
		uriSize+=components.length;
	}
	
	public Uri(String[] components) throws BadUriException
	{
		this.components = components;
		if(components.length < 1)
			throw new BadUriException("Too short URI!");
		int i;
		uriRepresentation = "";
		for(i = 0; i < components.length; i++)
		{
			uriSize+=components[i].length();
			uriRepresentation = uriRepresentation.concat("/" + components[i]);
		}
	}
	
	public String[] getComponents()
	{
		return components;
	}
	
	/*
	 * anotherUri = /polito.it/video/*
	 * thisUri = /polito.it/video/example.avi
	 * return true
	 */
	public boolean match(Uri anotherUri)
	{
		String[] s = anotherUri.getComponents();
		int i;
		for(i = 0; i < s.length && i < components.length; i++)
		{
			if(s[i].equals("*") || components[i].equals("*"))
				return true;
			if(!components[i].equals(s[i]))
				return false;
		}
		if(s.length == components.length)
			return true;
		else
			return false;
	}
	
	// Only for testing
	public int getFragmentNumber()
	{
		String s = components[components.length-1];
		if(!s.startsWith("s"))
			return -1;
		try
		{
			s = (String) s.subSequence(1, s.length());
			int result = Integer.parseInt(s);
			return result;
		} catch(Exception e) { return -1; }
	}
	
	public int getUriSizeInByte()
	{
		return uriSize;
	}
	
	public void setUriSizeInByte(int size)
	{
		this.uriSize = size;
	}
	
	public void initArray(int totalBytes, int maxPayload)
	{
		int numChunks = (int) (totalBytes / maxPayload);
		int lastDataPacketDim = (int) totalBytes % maxPayload;
		if(lastDataPacketDim != 0)
			++numChunks;
		Assert.assertTrue(numChunks > 0);
		array = new String[numChunks];
		int d;
		for(d=0; d < array.length; d++)
			array[d] = id + "/" + d;
		if(!hashEnabled)
			return;
		int k = 0;
		byte salt = 0;
		hashes = new int[numChunks][BloomFilter.HASHES];
		int[] res;
		for(int s = 0; s<array.length; s++)
		{
			res = new int[BloomFilter.HASHES];
			while (k < BloomFilter.HASHES) {
	            byte[] digest;
	            digestFunction.update(salt);
	            salt++;
	            digest = digestFunction.digest(array[s].getBytes());
	
	            for (int i = 0; i < digest.length/4 && k < BloomFilter.HASHES; i++) {
	                int h = 0;
	                for (int j = (i*4); j < (i*4)+4; j++) {
	                    h <<= 8;
	                    h |= ((int) digest[j]) & 0xFF;
	                }
	                res[k] = h;
	                k++;
	            }
	        }
			hashes[s] = res;
		}
	}
	
	public String getUriWithFragmentNumber(int fragment)
	{
		if(fragment < array.length)
			return array[fragment];
		else
			return id + "/" + fragment;
	}
	
	// Il ramo else viene visitato in caso di uri relativo a pacchetti ritrasmessi!
	public int[] getHashes(int fragment)
	{
		if(fragment < hashes.length)
			return hashes[fragment];
		else
		{
			int k = 0;
			byte salt = 0;
			while (k < BloomFilter.HASHES) 
			{
	            byte[] digest;
	            digestFunction.update(salt);
	            salt++;
	            digest = digestFunction.digest(getUriWithFragmentNumber(fragment).getBytes());
	
	            for (int i = 0; i < digest.length/4 && k < BloomFilter.HASHES; i++) 
	            {
	                int h = 0;
	                for (int j = (i*4); j < (i*4)+4; j++) {
	                    h <<= 8;
	                    h |= ((int) digest[j]) & 0xFF;
	                }
	                res[k] = h;
	                k++;
	            }
	        }
			return res;
		}
	}
	
	/* Returns a slash-separated representation of the URI components */
	public String toString()
	{
		return uriRepresentation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void setId(int id) {
		this.id = String.valueOf(id);
	}
	
	@Override
	public int hashCode() {
		int i;
		short s;
		String result = "";
		for(i=0; i<uriRepresentation.length() && i<3; i++)
		{
			s = (short) uriRepresentation.charAt(i);
			result = result.concat(String.valueOf(s));
		}
		return Integer.parseInt(result);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Uri)
		{
			Uri uri = (Uri) obj;
			if(this.toString().equals(uri.toString()))
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public static Uri buildStandardUri(String id)
	{
		String[] components;
		if(id.equals("default"))
		{
			components = new String[2];
			components[0] = "it";
			components[1] = "*";
		}
		else
		{
			components = new String[3];
			components[0] = "it";
			components[1] = id;
			components[2] = "*";
		}
		try {
			Uri u = new Uri(components);
			return u;
		} catch (BadUriException e) {
			e.printStackTrace();
			return null;
		}
	}

}
