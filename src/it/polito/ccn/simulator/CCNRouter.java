package it.polito.ccn.simulator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.CCNRouterG;
import it.polito.ccn.simulator.gui.RouterData;

public abstract class CCNRouter extends CCNNode implements PacketProcessor{
	
	protected Logger logger;
	protected FIB fib;
	protected PIT pit;
	protected Cache cache;
	protected CCNRouterG routerG;
	protected int minLifeTime = Integer.MAX_VALUE;
	protected List<Interface> interfaceList = new ArrayList<Interface>();
	protected static DecimalFormat dF = new DecimalFormat("#.#########");
	protected RouterData rData;
	protected QueueSize qSize;
	
	static
	{
		dF = new DecimalFormat("#.#########");
		DecimalFormatSymbols d = DecimalFormatSymbols.getInstance();
		d.setDecimalSeparator('.');
		dF.setDecimalFormatSymbols(d);
	}
	
	public CCNRouter(CCNNetwork network, String id, List<Link> links) {
		super(network, id, links);
		logger = new Logger(Logger.VERBOSE, "logs/router"+id+".dat");
		rData = new RouterData();
		rData.setId(id);
	}

	public abstract void entryTimedOut(Uri uri, int fragmentNumber, String pitIndex) throws LinkBusyException;
	
	public abstract RouterData getRouterData();
	
	protected void printInfo()
	{
		double mem = pit.getPitSize()/Math.pow(10, 6);
		System.out.println("************** STATS ROUTER "+id+" **************");
		if(routerG != null)
			System.out.println("Total PIT memory = "+routerG.getPitMemory()+" MB");
		System.out.println("PIT entry = "+pit.getElementsInPIT());
		System.out.println("PIT memory (occupied) = "+mem+" MB");
		System.out.println("Interfaces' number = " + this.interfaceList.size());
		System.out.println("Minimum LifeTime set = " + minLifeTime);
		cache.printInfo();
	}
	
	@Override
	public void terminate() {
		try {
			logger.stopAndWait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		printInfo();
	}
	
	protected Interface findInterfaceFromLink(Link link) {
		Iterator<Interface> i = interfaceList.iterator();
		while(i.hasNext())
		{
			Interface iFace = i.next();
			if(iFace.getLink().getId().equals(link.getId()))
				return iFace;
		}
		return null;
	}
	
	public Link getLinkFromId(List<Link> links, String id)
	{
		Iterator<Link> it = links.iterator();
		while(it.hasNext())
		{
			Link l = it.next();
			if(l.getId().equals(id))
				return l;
		}
		System.err.println("No LINK!");
		return null;
	}
	
	// public only for debug purposes
	public Interface getInterfaceFromId(String id)
	{
		Iterator<Interface> it = interfaceList.iterator();
		Interface face;
		while(it.hasNext())
		{
			face = it.next();
			if(face.getId().equals(id))
				return face;
		}
		System.err.println("No INTERFACE!");
		return null;
	}

}
