package it.polito.ccn.simulator;

import java.util.Comparator;

/* Criterio di confronto degli eventi: il piu' vecchio ha la priorita' */
public class EventComparator implements Comparator<Event>{

	@Override
	public int compare(Event e1, Event e2) {
		try{
			if(e1.getTimestamp() < e2.getTimestamp())
				return -1;
			if(e1.getTimestamp() > e2.getTimestamp())
				return 1;
			return 0;
		} 
		catch(Exception ex)
		{
			if(e1 == null)
				return 1;
			if(e2 == null)
				return -1;
			return 0;
		}
	}

}
