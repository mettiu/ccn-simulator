package it.polito.ccn.simulator.exception;

public class CounterOverflowException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public CounterOverflowException(String message)
	{
		super(message);
	}

}
