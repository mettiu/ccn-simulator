package it.polito.ccn.simulator.exception;

public class BadUriException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BadUriException(String message) {
		super(message);
	}

}
