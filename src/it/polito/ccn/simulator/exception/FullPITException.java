package it.polito.ccn.simulator.exception;

public class FullPITException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FullPITException(String message)
	{
		super(message);
	}

}
