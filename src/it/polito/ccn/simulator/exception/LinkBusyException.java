package it.polito.ccn.simulator.exception;

public class LinkBusyException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LinkBusyException(String message)
	{
		super(message);
	}

}
