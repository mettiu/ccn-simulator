package it.polito.ccn.simulator;

import it.polito.ccn.simulator.generated.InterfacesG.InterfaceG;

public class Interface 
{	
	private String id;
	private Link link;
	
	public Interface(InterfaceG next) {
		this.id = String.valueOf(next.getInterfaceNumber());
	}

	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public Link getLink()
	{
		return link;
	}
	
	public void setLink(Link link)
	{
		this.link = link;
	}

}
