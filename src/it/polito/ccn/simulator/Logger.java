package it.polito.ccn.simulator;

import java.text.DecimalFormat;
import java.util.concurrent.LinkedBlockingQueue;

public class Logger{
	
	/* Available logging LEVELS */
	public static final int EVER = -1;
	public static final int NONE = 0;
	public static final int INFO = 1;
	public static final int WARNING = 2;
	public static final int VERBOSE = 3;
	
	/* Logging level */
	private final int logLevel;
	/* Path file di log */
	//private final String log_file = "log.txt";
	
	private LinkedBlockingQueue<String> messagesQueue;
	private LoggerThread logger;
	private Thread t;
	private DecimalFormat dF;
	
	public Logger(int logLevel, String log_file)
	{
		this.logLevel = logLevel; 
		messagesQueue = new LinkedBlockingQueue<String>();
		logger = new LoggerThread(messagesQueue, log_file);
		dF = new DecimalFormat("#.########");
		t = new Thread(logger);
		t.start();
	}
	
	public void log(String toLog, double timestamp, int logLevel)
	{
		if(logLevel <= this.logLevel && !messagesQueue.offer("[" + dF.format(timestamp) + "] " + toLog))
			System.out.println("Unable to log " + toLog);
	}
	
	public void stopAndWait() throws InterruptedException
	{
		logger.stopLog();
		t.interrupt();
		t.join();
	}
	
	public static int decodeLogLevel(String logLevel)
	{
		if(logLevel == null || logLevel.equals(""))
			return 0;
		if(logLevel.equals("NONE"))
			return 0;
		else if(logLevel.equals("WARNING"))
			return 1;
		else if(logLevel.equals("INFO"))
			return 2;
		else if(logLevel.equals("VERBOSE"))
			return 3;
		else if(logLevel.equals("EVER"))
			return -1;
		else
		{
			System.err.println("Invalid logLevel! Logging disabled! Check your cfg file.");
			return 0;
		}
	}

}
