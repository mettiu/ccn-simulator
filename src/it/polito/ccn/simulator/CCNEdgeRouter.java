package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;
import it.polito.ccn.simulator.exception.FullQueueException;
import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.LinkG;
import it.polito.ccn.simulator.generated.InterfacesG.InterfaceG;
import it.polito.ccn.simulator.gui.RouterData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;

import org.apache.commons.math3.distribution.ExponentialDistribution;

public class CCNEdgeRouter extends CCNRouter{
	
	private final double USER_UPLINK_BAND = 0.001;
	private final double USER_DOWNLINK_BAND = 0.007;
	
	private double meanFileSize;
	private double meanInterarrivalTime;
	private double rate;
	private double totalDownloadTime = 0;
	private double retransmissionRate;
	
	private long totalBytesDownloaded = 0;
	
	private int downloadRequests = 0;
	private int departures = 0;
	private int aInteger = 0;
	private int users;
	private int finishedDownloads;
	
	private ExponentialDistribution exp;
	
	/* Upstream interface towards the backbone router */
	private Interface i;
	
	/* 
	 * This router is located between clients and backbone routers and it has:
	 * - infinite PIT
	 * - no cache 
	 * - service time is zero
	 * - infinite Buffer in/out
	 */
	public CCNEdgeRouter(CCNNetwork network, String id, List<Link> links, int clients, double rate)
	{
		super(network, id, links);
		Assert.assertTrue(links.size() == 1);
		Assert.assertTrue(links.get(0) != null);
		Assert.assertTrue(rate > 0);
		this.rate = rate;
		System.out.println(id + " - Number of clients = " + clients);
		users = 0;
		finishedDownloads = 0;
		retransmissionRate = 0;
		serviceTime = 0;
		fib = new FIB();
		/* PIT with infinite memory */
		pit = new SimplePIT(10000000);
		/* Cache NOT active */
		cache = new Cache();
		/* Interface connected to the POP */
		InterfaceG interf = new InterfaceG();
		interf.setInterfaceNumber(0);
		interf.setLink(links.get(0).getId());
		/* Default route */
		i = new Interface(interf);
		i.setLink(links.get(0));
		fib.addRoute(Uri.buildStandardUri("default"), i);
		links.get(0).addNode(this);
		interfaceList.add(i);
		/* Infinite queue */
		ingressQueues.put(links.get(0).getId(), new Queue());
		egressQueues.put(links.get(0).getId(), new Queue());
		/* 
		 * File size is computed taking into account the probability that the file is choosen from clients
		 */
		int j = 0;
		for(j=0; j<network.getDestinationFiles().size(); j++)
			meanFileSize += (network.getZipfDistribution().getProbability(j)*network.getDestinationFiles().get(j).getNumBytes());
		System.out.println(id + " - Mean file size = " + meanFileSize);
		network.getLogger().log(id + " - Mean file size = " + meanFileSize, CCNNetwork.getTime(), Logger.EVER);
		meanInterarrivalTime = meanFileSize/(rate*Math.pow(10, 6)/8);
		meanInterarrivalTime /= clients;
		System.out.println(id + " - Mean inter-arrival time = " + meanInterarrivalTime);
		network.getLogger().log(id + " - Mean inter-arrival time = " + meanInterarrivalTime, CCNNetwork.getTime(), Logger.EVER);
		exp = new ExponentialDistribution(meanInterarrivalTime);
		network.manageInterArrivalTime(meanInterarrivalTime);
		Event e = new Event(CCNNetwork.getTime() + exp.sample(), Event.Type.USER_ARRIVAL, null, this, null);
		network.addEvent(e);
	}
	
	public void incrementRequests() { downloadRequests++; }
	
	public void incrementDepartures() { departures++; }
	
	/*
	 * Add a user to this edge router.
	 */
	public void addUser()
	{
		users++;
		double arrivalTime = exp.sample();
		/* Schedule the next arrival */
		Event e = new Event(CCNNetwork.getTime() + arrivalTime, Event.Type.USER_ARRIVAL, null, this, null);
		network.addEvent(e);
		CCNRegularClient user;
		List<Link> list;
		InterfaceG interf = new InterfaceG();
		interf.setInterfaceNumber(++aInteger);
		interf.setLink(id+"link"+aInteger);
		Interface i = new Interface(interf);
		LinkG lG = new LinkG();
		lG.setId(id+"link"+aInteger);
		lG.setDelay(0.0);
		/* 
		 * Each user has 7Mbps/1Mbps downlink/uplink band
		 */
		lG.setDown(USER_DOWNLINK_BAND);
		lG.setUp(USER_UPLINK_BAND);
		lG.setNode1(id+"client"+aInteger);
		lG.setNode2(id);
		Link l = new Link(lG);
		l.addNode(this);
		i.setLink(l);
		interfaceList.add(i);
		ingressQueues.put(l.getId(), new Queue());
		egressQueues.put(l.getId(), new Queue());
		list = new ArrayList<Link>();
		list.add(l);
		user = new CCNRegularClient(network, id+"client"+aInteger, list, rate, this, i);
		user.reset();
		network.getUsers().add(user);
		network.addEvent(new Event(CCNNetwork.getTime(), Event.Type.SILENCE_FINISHED, l, user, null));
	}
	
	void destroyUser(CCNRegularClient user, Interface iFace, ClientStats stats)
	{
		Assert.assertNotNull(iFace);
		network.manageDownloadTermination(stats);
		/* Only when the net is stable */
		if(network.isSteady()) {
			finishedDownloads++;
			retransmissionRate += stats.getRetransmissionRate();
			totalBytesDownloaded += stats.getBytesDownloaded();
			totalDownloadTime += stats.getDownloadTime();
		}
		/* Link utilization stats */
		/*Link l = iFace.getLink();
		System.out.println("Link " + l.getId());
		Iterator<CCNNode> i = l.getNodes().iterator();
		CCNNode n;
		while(i.hasNext())
		{
			n = i.next();
			System.out.println("       Node " + n.getId() + " -> Utilization " + l.getLinkUtilization(n.getId()) + "/" + stats.getDownloadTime());
		}*/
		network.getUsers().remove(user);
		interfaceList.remove(iFace);
		iFace.setLink(null);
		iFace = null;
		user = null;
		users--;
	}
	
	public PIT getPIT() { return pit; }
	
	public double getServiceTime() { return serviceTime; }
	
	public List<Routable> processPacket(Event e)
	{
		cpuBusy.put(e.getLink().getId(), true);
		Iterator<Interface> it = interfaceList.iterator();
		Interface i = null;
		/* Look for arrival interface */
		while(it.hasNext())
		{
			i = it.next();
			if(i.getLink().getId().equals(e.getLink().getId()))
				break;
		}
		Assert.assertNotNull(i);
		if(e.getPackt() instanceof CCNInterest)
		{
			/* 
			 * INTEREST PROCESSING 
			 * Visiting order -> Cache (disabled), PIT, FIB
			 */
			CCNInterest interest = (CCNInterest) e.getPackt();
			/* Look in the PIT */
			if(pit.existsUri(interest.getUri(), interest.getFragmentNumber()))
			{
				try 
				{
					boolean res = pit.addEntry(interest.getUri(), interest.getFragmentNumber(), i);
					Assert.assertTrue(res); // Already available entry. Insertion must not fail.
					return null;
				} catch (FullPITException e1) {
					System.err.println(e1);
					return null;
				}
			}
			/* FIB lookup */
			List<Interface> iList = fib.findRoute(interest.getUri());
			if(iList != null)
			{
				List<Routable> l = new ArrayList<Routable>();
				Iterator<Interface> iter = iList.iterator();
				while(iter.hasNext())
				{
					Interface iFace = iter.next();
					try 
					{
						if(pit.addEntry(interest.getUri(), interest.getFragmentNumber(), findInterfaceFromLink(e.getLink())))
						{
							l.add(new RoutingResult(interest, iFace.getLink(), this));
							Event event = new Event(CCNNetwork.getTime() + interest.getInterestLifeTime(), Event.Type.PIT_ENTRY_TIMEOUT, this, interest.getUri());
							event.setPackt(interest);
							network.addEvent(event);
							pit.addTimeoutEvent(interest.getUri(), interest.getFragmentNumber(), event);
							return l;
						}
						else
						{
							//TODO: Should never happen because this PIT is really BIG. Handle this case anyway!
							System.err.println(id + "Edge router's PIT full!");
							interest.getOrigin().deletePacketFromPipeline();
							network.incrementRetransmissions();
							network.getLogger().log("PIT satura -> "+pit.getElementsInPIT()+" entries.", CCNNetwork.getTime(), Logger.INFO);
						}
					} catch (FullPITException e1) {
						interest.getOrigin().deletePacketFromPipeline();
						network.incrementRetransmissions();
						network.getLogger().log("PIT satura -> "+pit.getElementsInPIT()+" entries.", CCNNetwork.getTime(), Logger.INFO);
					}
				}
			}
			else
				System.err.println("Should never happen"); // There is always a route
			return null;
		}
		else if(e.getPackt() instanceof CCNContentObject)
		{
			/* Pit, Cache */
			CCNContentObject object = (CCNContentObject) e.getPackt();
			if(pit.existsUri(object.getUri(), object.getFragmentNumber()))
			{
				/* Some clients were waiting this content: I forward it! */
				Event e1 = pit.getTimeoutEvent(object.getUri(), object.getFragmentNumber());
				if(e1 != null)
					e1.setCancelled(true);
				else
					System.out.println("Strange!");
				pit.removeTimeoutEvent(object.getUri(), object.getFragmentNumber());
				List<Interface> l = pit.getInterfacesPending(object.getUri(), object.getFragmentNumber());
				if(l != null)
				{
					if(l.contains(i))
						l.remove(i);
					it = l.iterator();
					List<Routable> rR = new ArrayList<Routable>();
					Interface iFace;
					while(it.hasNext())
					{
						iFace = it.next();
						rR.add(new RoutingResult(object, iFace.getLink(), this));
					}
					pit.removeEntry(object.getUri(), object.getFragmentNumber());
					return rR;
				}
				pit.removeEntry(object.getUri(), object.getFragmentNumber());
			}
			return null;
		}
		else
			return null;
	}

	/* 
	 * Only edge routers handle their clients pipeline. 
	 */
	public void entryTimedOut(Uri uri, int fragmentNumber, String pitIndex) throws LinkBusyException {
		Interface iFace;
		Iterator<CCNNode> i2;
		CCNNode node;
		CCNRegularClient user;
		Iterator<Interface> i;
		List<Interface> list;
		Routable r;
		CCNNode source;
		double transmission_time;
		list = pit.getInterfacesPending(uri, fragmentNumber);
		if(list != null)
		{
			i = list.iterator();
			while(i.hasNext())
			{
				iFace = i.next();
				i2 = iFace.getLink().getNodes().iterator();
				while(i2.hasNext())
				{
					node = i2.next();
					if(node instanceof CCNRegularClient)
					{
						user = (CCNRegularClient) node;
						r = user.deletePacketFromPipeline();
						if(r != null)
						{
							Assert.assertTrue(r.getSource().getId().equals(user.getId()));
							source = r.getSource();
							network.incrementRetransmissions();
							if(source.isEmptyOut(r.getLink()) && !r.getLink().isBusy(source.getId()))
							{
								r.getLink().takeLink(source.getId());
								transmission_time = (r.getPacket().getSizeInByte())*8 / (r.getLink().getCapacity(source.getId()) * Math.pow(10, 6));
								network.addEvent(new Event(CCNNetwork.getTime() + transmission_time, Event.Type.TRANSMISSION_FINISHED, r.getLink(), r.getSource(), r.getPacket()));
							}
							else
							{
								/* ...otherwise enqueue the packet in the egress queue. */
								try {
									source.enqueueOut(r, r.getLink());
								}
								catch (FullQueueException e2) {
									e2.printStackTrace();
								}
							}
						}
					}
				}
			}
			pit.removeEntry(uri, fragmentNumber);
			pit.removeTimeoutEvent(uri, fragmentNumber);
		}
		else { }
			// System.err.println(id + ". Should never happen.");
	}
	
	protected void printInfo()
	{
		super.printInfo();
		System.out.println("Arrivals = " + downloadRequests);
		System.out.println("Departures = " + departures);
		System.out.println("Useful Bytes downloaded = " + totalBytesDownloaded);
		if(finishedDownloads != 0)
		{
			double temp = retransmissionRate / (double) finishedDownloads;
			System.out.println(id + " - Retransmission rate = " + temp + " %");
			network.getLogger().log(id + " - Retransmission rate = " + temp + " %", CCNNetwork.getTime(), Logger.EVER);
			temp = totalDownloadTime / (double )finishedDownloads;
			System.out.println(id + " - Mean download time = " + temp + " sec");
			network.getLogger().log(id + " - Retransmission rate = " + temp + " %", CCNNetwork.getTime(), Logger.EVER);
		}
	}

	@Override
	public RouterData getRouterData() {
		rData.setUsers(users);
		rData.setPitEntries(pit.getElementsInPIT());
		return rData;
	}

}
