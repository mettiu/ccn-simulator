package it.polito.ccn.simulator;

public interface Routable {
	
	public CCNPacket getPacket();
	public Link getLink();
	public CCNNode getSource();

}
