package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;

import java.util.List;

import org.apache.commons.math3.distribution.UniformRealDistribution;

/*
 * DiPIT implemented with the mathematical model of the Bloom filter.
 * For another implementation, look at the DiPITBF.java file.
 */
public class DiPIT extends PIT{
	
	private static UniformRealDistribution dist;
	
	private PIT p;
	private int falsePositives;
	private long totalQueries;
	private final long size;
	
	static{
		dist = new UniformRealDistribution();
	}

	public DiPIT(double pitSize) {
		super(pitSize);
		/* Use RegularPIT instead of HashedPIT to speed up performances */
		p = new SimplePIT(1000*pitSize); 			// No matter about this pit's size
		size = (long) (pitSize * Math.pow(10, 6)); 	// Used for prob calc
		falsePositives = 0;
		totalQueries = 0;
	}
	
	public int getElementsInPIT() { return p.getElementsInPIT(); }
	
	public int getFalsePositives() { return falsePositives; }
	
	public long getTotalQueries() { return totalQueries; }

	@Override
	public int getAverageEntrySize() {
		return p.getAverageEntrySize();
	}

	@Override
	public boolean existsUri(Uri uri, int fragmentNumber) {
		totalQueries++;
		if(p.existsUri(uri, fragmentNumber))
			return true;
		double prob = BloomFilter.getFalsePositiveProbability(p.getElementsInPIT(), size);
		double random = dist.sample();
		/* False positive */
		if(random <= prob)
		{
			falsePositives++;
			return true;
		}
		else
			return false;
	}

	@Override
	public List<Interface> getInterfacesPending(Uri uri, int fragmentNumber) {
		return p.getInterfacesPending(uri, fragmentNumber);
	}

	@Override
	public boolean addEntry(Uri uri, int fragmentNumber, Interface iFace)
			throws FullPITException {
		return p.addEntry(uri, fragmentNumber, iFace);
	}

	@Override
	public void removeEntry(Uri uri, int fragmentNumber) {
		p.removeEntry(uri, fragmentNumber);
	}
	
	public void clearAll()
	{
		p.reset();
	}
	
}
