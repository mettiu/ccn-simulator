package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;

public abstract class PIT {
	
	protected Map<String, PITEntry> pit;
	protected Map<String, Event> timeoutEvents;
	protected long pitSize; 	// Currently available memory -> In bytes
	protected final long maxSize;
	protected Interface iFace;
	
	public PIT(double pitSize) 	// in MBytes
	{
		Assert.assertTrue(pitSize > 0);
		// Converting from MBytes to bytes
		this.pitSize = (long) (pitSize * Math.pow(10, 6));
		this.maxSize = this.pitSize;
		pit = new HashMap<String, PITEntry>(100, (float) 0.75);
		timeoutEvents = new HashMap<String, Event>();
	}
	
	public abstract int getAverageEntrySize(); 	//in bytes
	public abstract boolean existsUri(Uri uri, int fragmentNumber);
	public abstract List<Interface> getInterfacesPending(Uri uri, int fragmentNumber);
	public abstract boolean addEntry(Uri uri, int fragmentNumber, Interface iFace) throws FullPITException;
	public abstract void removeEntry(Uri uri, int fragmentNumber);
	public abstract void clearAll();
	
	public long getPitSize() { return maxSize - pitSize; } 	// Returns the amount of occupied memory
	
	public int getTimeoutEvents() { return timeoutEvents.size(); }
	
	public long getFreePit() { return pitSize; }
	
	public int getElementsInPIT() { return pit.size(); }

	public Event getTimeoutEvent(Uri uri, int fragmentNumber) { return timeoutEvents.get(uri.getUriWithFragmentNumber(fragmentNumber)); }
	
	public void addTimeoutEvent(Uri uri, int fragmentNumber, Event event) { timeoutEvents.put(uri.getUriWithFragmentNumber(fragmentNumber), event); }
	
	public void removeTimeoutEvent(Uri uri, int fragmentNumber) { timeoutEvents.remove(uri.getUriWithFragmentNumber(fragmentNumber)); }
	
	public Interface getiFace() { return iFace; }

	public void setiFace(Interface iFace) { this.iFace = iFace; }
	
	public double getLoad() 
	{
		double t = (double)(maxSize - pitSize) / maxSize;
		return t;
	}
	
	public void reset()
	{
		pit.clear();
		timeoutEvents.clear();
		/* Restore the space */
		pitSize = maxSize;
	}
	
	public void printPit()
	{
		Iterator<Entry<String, PITEntry>> i = pit.entrySet().iterator();
		Entry<String, PITEntry> entry;
		System.out.println("******PIT*******");
		while(i.hasNext())
		{
			entry = i.next();
			System.out.println(entry.getKey());
		}
		System.out.println("******END PIT*******");
	}

}
