package it.polito.ccn.simulator;

import java.util.Random;

import org.junit.Assert;

public class ZipfDistribution 
{	
	/* Static data shared among instances */
	private static Double[] cdf;
	private static Double[] density;
	
	private Random random;
	private int totalElements;
	private double alpha;
	private double q;
	
	public ZipfDistribution(long seed, int totalElements, double alpha, double q)
	{
		this.totalElements = totalElements;
		this.alpha = alpha;
		this.q = q;
		random = new Random(seed);
		init();
	}
	
	private void init()
	{
		Assert.assertTrue(q > 0);
		if(cdf == null)
			cdf = new Double[totalElements];
		if(density == null)
			density = new Double[totalElements];
		double temp, norm=0;
		double sum=0;
		int i;
		for(i=0; i<totalElements; i++)
		{
			temp=(1.0/Math.pow(i+q, alpha));
			Assert.assertTrue(temp > 0);
			cdf[i]=temp;
			density[i]=temp;
			if(i!=0)
				cdf[i]+=cdf[i-1];
			norm+=temp;
		}
		for(i=0; i<totalElements; i++)
		{
			cdf[i] /= norm;
			density[i] /= norm;
			sum += density[i];
		}
		System.out.println("Normalizzazione = " + norm);
		System.out.println("Integrale funzione densita' = " + sum);
	}
	
	public int nextInt()
	{
		double r = random.nextDouble();
		int i;
		for(i=0; i<totalElements && r>=cdf[i]; i++);
		return i;
	}
	
	public double getProbability(int i)
	{
		if(i < 0 || i > totalElements)
			return 0.0;
		return density[i];
	}
	
	public Double[] getDensity() { return density; }
	
	public Double[] getCumulativeFunction() { return cdf; }

}