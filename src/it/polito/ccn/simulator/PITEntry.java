package it.polito.ccn.simulator;

import java.util.List;

public class PITEntry {
	
	private List<Interface> list;

	public List<Interface> getList() {
		return list;
	}

	public void setList(List<Interface> list) {
		this.list = list;
	}	

}
