//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.09.21 at 07:53:07 PM CEST 
//


package it.polito.ccn.simulator.usersgen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.polito.ccn.simulator.usersgen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UsersAllocation_QNAME = new QName("http://www.example.org/UsersAllocationSchema", "UsersAllocation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.polito.ccn.simulator.usersgen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UsersAllocation }
     * 
     */
    public UsersAllocation createUsersAllocation() {
        return new UsersAllocation();
    }

    /**
     * Create an instance of {@link POP }
     * 
     */
    public POP createPOP() {
        return new POP();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UsersAllocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.example.org/UsersAllocationSchema", name = "UsersAllocation")
    public JAXBElement<UsersAllocation> createUsersAllocation(UsersAllocation value) {
        return new JAXBElement<UsersAllocation>(_UsersAllocation_QNAME, UsersAllocation.class, null, value);
    }

}
