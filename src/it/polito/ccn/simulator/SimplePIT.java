package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;

import java.util.ArrayList;
import java.util.List;

public class SimplePIT extends PIT{
	
	public int n = 0;
	public int averageEntrySize = 0; //in bytes
	
	public SimplePIT(double pitSize)
	{
		super(pitSize);
	}
	
	public int getElementsInPIT() { return pit.size(); }
	
	@Override
	public int getAverageEntrySize() {
		return averageEntrySize;
	}
	
	public boolean existsUri(Uri uri, int fragmentNumber)
	{
		if(pit.containsKey(uri.getUriWithFragmentNumber(fragmentNumber)))
			return true;
		else
			return false;
	}
	
	public List<Interface> getInterfacesPending(Uri uri, int fragmentNumber)
	{
		try
		{
			List<Interface> list = pit.get(uri.getUriWithFragmentNumber(fragmentNumber)).getList();
			return list;
		} 
		catch(Exception e)
		{
			return null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.polito.ccn.simulator.PIT#addEntry(it.polito.ccn.simulator.Uri, int, it.polito.ccn.simulator.Interface)
	 * In caso di PIT piena ritorno false anzich� sollevare un'eccezione perch� � una gestione
	 * pi� veloce e leggera.
	 */
	public boolean addEntry(Uri uri, int fragmentNumber, Interface iFace) throws FullPITException
	{
		String stringKey = uri.getUriWithFragmentNumber(fragmentNumber);
		List<Interface> list;
		PITEntry entry = pit.get(stringKey);
		if(entry == null)
		{
			if(pitSize - uri.getUriSizeInByte() >= 0)
			{
				list = new ArrayList<Interface>();
				list.add(iFace);
				entry = new PITEntry();
				entry.setList(list);
				pit.put(stringKey, entry);
				pitSize-=uri.getUriSizeInByte();
				averageEntrySize = ((n*averageEntrySize) + uri.getUriSizeInByte()) / ++n;
				return true;
			}
			else
				return false;
		}
		else if(entry.getList().contains(iFace))
			return true;
		else
			entry.getList().add(iFace);
		return true;
	}
	
	public void removeEntry(Uri uri, int fragmentNumber)
	{
		/*List<Interface> list = pit.get(uri.toString());
		if(list == null)
			return;
		else
			System.out.println("[PIT] Should never happen.");*/
		pit.remove(uri.getUriWithFragmentNumber(fragmentNumber));
		pitSize+=uri.getUriSizeInByte();
		//Assert.assertTrue(pitSize>=0);
	}

	@Override
	public void clearAll() {
		pit.clear();
		timeoutEvents.clear();
		pitSize = maxSize;
	}

}
