package it.polito.ccn.simulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;

import it.polito.ccn.simulator.generated.PublisherG;
import it.polito.ccn.simulator.generated.PublisherG.LocalContentG;
import it.polito.ccn.simulator.gui.PublisherData;

public class CCNPublisher extends CCNNode
{	
	private final List<Uri> localContents;
	private boolean isAttacker;
	private Link link;
	private Queue in;
	private Queue out;
	private PublisherData data;

	public CCNPublisher(CCNNetwork network, String id, List<Link> links, PublisherG publisher) {
		super(network, id, links);
		Assert.assertTrue(links.size() == 1);
		this.link = links.get(0);
		this.serviceTime = 0;
		isAttacker = publisher.isAttacker();
		if(isAttacker)
			System.out.println("Attacker " + id);
		localContents = new ArrayList<Uri>();
		String linkId = publisher.getLink();
		/* Fill the Link handle (inherited from CCNNode) */
		Iterator<Link> i = links.iterator();
		Link l;
		while(i.hasNext())
		{
			l = i.next();
			if(l.getId().equals(linkId))
			{
				this.link = l;
				l.addNode(this);
				break;
			}
		}
		data = new PublisherData();
		data.setId(id);
		//TODO: Limit the queue size to avoid RAM consuption problems!!!!!
		// Queue size for content providers is fixed to 1000 MB = 1 GB
		QueueSize qSize = new QueueSize(1000);
		in = new Queue(qSize);
		out = new Queue(qSize);
		ingressQueues.put(link.getId(), in);
		egressQueues.put(link.getId(), out);
		/* Populate local contents repository */
		Iterator<LocalContentG> it = publisher.getLocalContentG().iterator();
		while(it.hasNext())
			localContents.add(Uri.buildStandardUri(it.next().getPrefix()));
	}
	
	public boolean isAttacker() { return isAttacker; }
	
	public List<Uri> getDestinations() { return localContents; }
	
	public boolean search(Uri toSearch)
	{
		Iterator<Uri> i = localContents.iterator();
		while(i.hasNext())
		{
			if(toSearch.match(i.next()))
				return true;
		}
		return false;
	}

	@Override
	public List<Routable> processPacket(Event e) {
		if(isAttacker)
		{
			e = null;
			return null;
		}
		/* Only process Interest packets*/
		Assert.assertTrue(e.getPackt() instanceof CCNInterest);
		CCNInterest interest = (CCNInterest) e.getPackt();
		/* Look for the content */
		//network.getLogger().log("Publisher: request for " + interest.getUri() + " ("+interest.getNonce()+")", network.getTime(), Logger.INFO);
		if(search(interest.getUri()))
		{
			/* Reply with the ContentObject */
			CCNContentObject object = new CCNContentObject(interest.getUri(), interest.getFragmentNumber(), network.getPacketHeaderSizeInByte(), interest.getResponseContentDim(), interest.isLastChunk(), interest);
			object.setCachable(interest.isCachable());
			List<Routable> result = new ArrayList<Routable>();
			RoutingResult rR = new RoutingResult(object, link, this);
			result.add(rR);
			//network.getLogger().log("Publisher: sparato pacchetto " + object.getUri() + "("+object.getNonce()+")", CCNNetwork.getTime(), Logger.INFO);
			interest = null;
			e = null;
			return result;
		}
		else
		{
			/* Should never happen (except DiPIT case) */
			Assert.assertTrue(1 == 2);
			interest = null;
			e = null;
			return null; /* I don't have the content */
		}
	}
	
	public PublisherData getPublisherData()
	{
		data.setOutElements(out.elementNumber());
		return data;
	}

	@Override
	public void terminate() {
		/* Do nothing */
	}

}
