package it.polito.ccn.simulator;

import java.util.List;

import org.junit.Assert;

import it.polito.ccn.simulator.generated.CCNRouterG;

public class CCNComponentFactory {
	
	/*
	 * N.B.
	 * Exploited by the attacker (CCNAttacker class)
	 * - Set to 1000 in case of RegularPIT.
	 * - Set to 7 in all the other cases. 
	 */
	private int attackerUriDim;
	
	private String routerType;
	private String pitType;
	
	public CCNComponentFactory(Integer attackerUriDim, String routerType, String pitType)
	{
		this.attackerUriDim = attackerUriDim;
		this.routerType = routerType;
		this.pitType = pitType;
		if(routerType.equals("DiPIT") && !(pitType.equals("DiPIT") || pitType.equals("DiPITBF") ))
		{
			System.err.println("Only DiPIT or DiPITBF are allowed for DiPITRouter. PITType set to DiPIT");
			pitType = "DiPIT";
		}
		if(routerType.equals("Core") && (pitType.equals("DiPIT") || pitType.equals("DiPITBF") ))
		{
			System.err.println("Core routers don't support DiPIT. PITType set to HashedPIT");
			pitType = "HashedPIT";
		}
		if(pitType.equals("HashedPIT"))
		{
			Assert.assertTrue(attackerUriDim == 7); 	// Otherwise the attack is not optimized
		}
		// Other checks... 
	}
	
	public int getAttackerUriDim() { return attackerUriDim; }
	
	public CCNRouter getRouter(CCNNetwork network, String id, List<Link> links, CCNRouterG ccnRouter)
	{
		if(routerType.equals("DiPIT"))
			return new CCNDiPITRouter(network, id, links, ccnRouter);
		else if(routerType.equals("Core"))
			return new CCNCoreRouter(network, id, links, ccnRouter);
		else
		{
			System.err.println("Unknow router type. RouterType set to Core");
			return new CCNCoreRouter(network, id, links, ccnRouter);
		}
	}
	
	public PIT getPIT(double pitSize)
	{
		if(pitType.equals("DiPIT"))
			return new DiPIT(pitSize);
		else if(pitType.equals("DiPITBF"))
			return new DiPITBF(pitSize);
		else if(pitType.equals("SimplePIT"))
			return new SimplePIT(pitSize);
		else if(pitType.equals("HashedPIT"))
			return new HashedPIT(pitSize);
		else
		{
			System.err.println("[ERR] Unknow pit type. PitType set to HashedPIT by default.");
			return new HashedPIT(pitSize);
		}
	}

}
