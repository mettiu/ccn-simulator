package it.polito.ccn.simulator;

import java.util.List;

public interface PacketProcessor {
	
	public List<Routable> processPacket(Event e);
	public double getServiceTime();

}
