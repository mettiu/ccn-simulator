package it.polito.ccn.simulator;

import java.util.UUID;

public abstract class CCNPacket {
	
	protected int nonce;
	protected boolean cachable = true;
	protected Uri uri;
	protected int fragmentNumber;
	protected int headerSizeInByte;
	
	protected CCNPacket(Uri uri, int fragmentNumber, int headerSizeInByte)
	{
		this.uri = uri;
		this.fragmentNumber = fragmentNumber;
		this.headerSizeInByte = headerSizeInByte;
		this.nonce = Math.abs(UUID.randomUUID().hashCode());
	}
	
	public int getFragmentNumber() { return fragmentNumber; }
	
	public void setFragmentNumber(int fragmentNumber) { this.fragmentNumber = fragmentNumber; }
	
	public Uri getUri() { return uri; }

	public int getNonce() { return nonce; }
	
	public boolean isCachable() { return cachable; }
	
	public void setCachable(boolean cachable) { this.cachable = cachable; }
	
	public abstract int getSizeInByte();

}
