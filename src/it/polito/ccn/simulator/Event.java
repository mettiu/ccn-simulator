package it.polito.ccn.simulator;

import java.util.List;

public class Event {
	
	public static enum Type {
		USER_ARRIVAL, 
		NEW_INTEREST, 
		TRANSMISSION_FINISHED, 
		PACKET_ARRIVAL, 
		SERVICE_FINISHED, 
		SILENCE_FINISHED, 
		PIT_ENTRY_TIMEOUT, 
		RESEND_INTEREST,
		REFRESH_PIT,
		PERIODIC_CHECK;
	}
	
	private double timestamp;
	private Type type;
	private Link link;
	private CCNNode source;
	private CCNPacket pckt;
	private Uri uri;
	/* Only used in SERVICE_FINISHED event (it contains routing result) */
	private List<Routable> routable;
	/* Only used in PIT_ENTRY_TIMEOUT event */
	private boolean cancelled;
	/* Only used with DiPIT */
	private String pitIndex;
	
	public Event(double timestamp, Type type, Link link, CCNNode source, CCNPacket pckt)
	{
		this.timestamp = timestamp;
		this.type = type;
		this.link = link;
		this.source = source;
		this.pckt = pckt;
		this.setCancelled(false);
	}
	
	public Event(double timestamp, Type type, CCNNode source, Uri uri)
	{
		this.timestamp = timestamp;
		this.type = type;
		this.source = source;
		this.uri = uri;
		this.setCancelled(false);
	}
	
	public Type getType() { return this.type; }
	
	public void setType(Type type) { this.type = type; }
	
	public List<Routable> getRoutable() { return routable; }

	public void setRoutable(List<Routable> routable) { this.routable = routable; }
	
	public CCNPacket getPackt() { return pckt; }
	
	public void setPackt(CCNPacket pckt) { this.pckt = pckt; }
	
	public double getTimestamp() { return timestamp; }
	
	public void setTimestamp(double timestamp) { this.timestamp = timestamp; }
	
	public Link getLink() {	return this.link; }
	
	public void setLink(Link link) { this.link = link; }
	
	public CCNNode getSource() { return this.source; }
	
	public void setSource(CCNNode source) { this.source = source; }

	public Uri getUri() { return uri; }

	public void setUri(Uri uri) { this.uri = uri; }

	public boolean isCancelled() { return cancelled; }

	public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }

	public String getPitIndex() { return pitIndex; }

	public void setPitIndex(String pitIndex) { this.pitIndex = pitIndex; }

}
