package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;
import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.CCNRouterG;
import it.polito.ccn.simulator.generated.FibG.FibEntry;
import it.polito.ccn.simulator.generated.InterfacesG.InterfaceG;
import it.polito.ccn.simulator.gui.RouterData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;

public class CCNCoreRouter extends CCNRouter{
	
	private int pitElements;
	
	//debug
	@SuppressWarnings("unused")
	private boolean debug = false; 	// Set to TRUE to activate debug mode
	@SuppressWarnings("unused")
	private int processedPackets = 0;
	private int load = 0; 			// Measured as Interests per second
	private int processedInterests = 0;
	private double elapsedTime = 0;
	private Logger loadLogger;
	
	public CCNCoreRouter(CCNNetwork network, String id, List<Link> links, CCNRouterG ccnRouter)
	{
		super(network, id, links);
		Assert.assertTrue(links.size() >= 1);
		if(id.equals("rm") || id.equals("mi"))
			debug = true;
		this.routerG = ccnRouter;
		this.pitElements = -100;
		serviceTime = ccnRouter.getServiceTime();
		fib = new FIB();
		pit = network.getFactory().getPIT(ccnRouter.getPitMemory());
		cache = new Cache(network, ccnRouter.getCacheMemory());
		qSize = new QueueSize((int)(ccnRouter.getPacketsQueueMemory()*Math.pow(10, 6)));
		Iterator<InterfaceG> iter = ccnRouter.getInterfacesG().getInterfaceG().iterator();
		while(iter.hasNext())
		{
			InterfaceG iface = iter.next();
			Interface i = new Interface(iface);
			Link l = getLinkFromId(links, iface.getLink());
			ingressQueues.put(l.getId(), new Queue(qSize));
			egressQueues.put(l.getId(), new Queue(qSize));
			l.addNode(this);
			i.setLink(l);
			interfaceList.add(i);
		}
		/* Populate router FIB */
		Iterator<FibEntry> it = ccnRouter.getFibG().getFibEntry().iterator();
		while(it.hasNext())
		{
			FibEntry entry = it.next();
			Uri uri = Uri.buildStandardUri(entry.getPrefix());
			Iterator<Integer> i = entry.getInterface().iterator();
			while(i.hasNext())
				fib.addRoute(uri, getInterfaceFromId(String.valueOf(i.next())));
		}
		//fib.printFIB();
		loadLogger = new Logger(Logger.VERBOSE, "logs/loadrouter"+id);
	}
	
	public PIT getPIT() { return pit; }
	
	public List<Routable> processPacket(Event e)
	{
		cpuBusy.put(e.getLink().getId(), true);
		processedPackets++;
		if(pit.getElementsInPIT() > pitElements+40 || pit.getElementsInPIT() < pitElements-40)
		{
			logger.log(dF.format(CCNNetwork.getTime()) + "\t" + pit.getElementsInPIT() + "\t" + pit.getPitSize()/Math.pow(10, 6), CCNNetwork.getTime(), Logger.VERBOSE);
			pitElements = pit.getElementsInPIT();
		}
		Iterator<Interface> it = interfaceList.iterator();
		Interface i = null, temp;
		while(it.hasNext())
		{
			temp = it.next();
			if(temp.getLink().getId().equals(e.getLink().getId()))
			{
				i = temp;
				break;
			}
		}
		if(i == null)
		{
			System.err.println("[ERR] " + id + " - No arrival interface for link " + e.getLink().getId() + ". Exiting...");
			//System.exit(-1);
		}
		if(e.getPackt() instanceof CCNInterest)
		{
			processedInterests++;
			if(CCNNetwork.getTime() - elapsedTime > 1)
			{
				load = processedInterests;
				processedPackets = 0;
				processedInterests = 0;
				elapsedTime = CCNNetwork.getTime();
				loadLogger.log(CCNNetwork.getTime() + "\t" + load, CCNNetwork.getTime(), Logger.VERBOSE);
			}
			CCNInterest interest = (CCNInterest) e.getPackt();
			//TODO: Future work: develop a Lifetime Handler
			/* LifeTime Handler */
			//EXAMPLE1:
			/*if(interest.getInterestLifeTime() > network.getDefaultInterestLifeTime() && pit.getLoad() > 0.5)
			{
				interest.setInterestLifeTime(network.getDefaultInterestLifeTime());
				if(minLifeTime > network.getDefaultInterestLifeTime())
					minLifeTime = network.getDefaultInterestLifeTime();
			}*/
			//EXAMPLE2:
			/*if(interest.getInterestLifeTime() > network.getDefaultInterestLifeTime() && pit.getLoad() > 0.5)
			{
				long f = pit.getFreePit();
				// LT = Free_mem / (Interest_arrival_rate * Average_entry_size)
				if(pit.getAverageEntrySize() != 0 && load != 0)
				{
					double temp = (f/(load*pit.getAverageEntrySize()));
					if(temp < 1)
						temp = 1;
					if(temp < minLifeTime)
						minLifeTime = (int)temp;
					interest.setInterestLifeTime((int)temp);
				}
			}*/
			if(interest.isCachable())
			{
				int result = cache.searchObject(interest.getUri(), interest.getFragmentNumber());
				if(result > 0)
				{
					/* Not found in cache */
					CCNContentObject object = new CCNContentObject(interest.getUri(), interest.getFragmentNumber(), network.getPacketHeaderSizeInByte(), result, interest.isLastChunk(), interest);
					RoutingResult rR = new RoutingResult(object, e.getLink(), this);
					List<Routable> l = new ArrayList<Routable>();
					l.add(rR);
					return l;
				}
			}
			/* Look in the PIT */
			if(pit.existsUri(interest.getUri(), interest.getFragmentNumber()))
			{
				try 
				{
					boolean res = pit.addEntry(interest.getUri(), interest.getFragmentNumber(), i);
					/* Entry is already there so the insertion cannot fail */
					Assert.assertTrue(res);
					return null;
				} catch (FullPITException e1) {
					System.err.println("Should never happen!" + e1);
					return null;
				}
			}
			/* FIB lookup */
			List<Interface> iList = fib.findRoute(interest.getUri());
			if(iList != null)
			{
				List<Routable> l = new ArrayList<Routable>();
				Iterator<Interface> iter = iList.iterator();
				while(iter.hasNext())
				{
					Interface iFace = iter.next();
					try 
					{
						if(pit.addEntry(interest.getUri(), interest.getFragmentNumber(), findInterfaceFromLink(e.getLink())))
						{
							l.add(new RoutingResult(interest, iFace.getLink(), this));
							Event event = new Event(CCNNetwork.getTime() + interest.getInterestLifeTime(), Event.Type.PIT_ENTRY_TIMEOUT, this, interest.getUri());
							event.setPackt(interest);
							network.addEvent(event);
							pit.addTimeoutEvent(interest.getUri(), interest.getFragmentNumber(), event);
							return l;
						}
						else
						{
							/* No specific action for backbone routers */
							return null;
						}
					} catch (FullPITException e1) {
						//System.err.println(id + e1);
						//network.getLogger().log("PIT satura -> "+pit.getElementsInPIT()+" entries.", network.getTime(), Logger.INFO);
					}
				}
			}
			else
				System.err.println("Should never happen"); // A route is always available
			return null;
		}
		else if(e.getPackt() instanceof CCNContentObject)
		{
			// Pit, Cache
			CCNContentObject object = (CCNContentObject) e.getPackt();
			/*if(object.isCachable())	// CACHE DISABLED
			{
				if(cache.searchObject(object.getName()) != 0)
					return null;
			}
			else
				System.out.println("Retransmitted object in transit");*/
			if(pit.existsUri(object.getUri(), object.getFragmentNumber()))
			{
				/* Put te object in cache */
				//if(object.isCachable())
					//cache.addElement(object.getUri(), object.getFragmentNumber(), object.getSizeInByte());
				/* Some clients were waiting for this content: forward it! */
				Event e1 = pit.getTimeoutEvent(object.getUri(), object.getFragmentNumber());
				if(e1 != null)
					e1.setCancelled(true);
				else
					System.out.println("Strange!");
				pit.removeTimeoutEvent(object.getUri(), object.getFragmentNumber());
				List<Interface> l = pit.getInterfacesPending(object.getUri(), object.getFragmentNumber());
				if(l != null)
				{
					if(l.contains(i))
						l.remove(i);
					it = l.iterator();
					List<Routable> rR = new ArrayList<Routable>();
					while(it.hasNext())
					{
						Interface iFace = it.next();
						rR.add(new RoutingResult(object, iFace.getLink(), this));
					}
					pit.removeEntry(object.getUri(), object.getFragmentNumber());
					return rR;
				}
				pit.removeEntry(object.getUri(), object.getFragmentNumber());
			}
			else
			{
				//System.err.println("Uri non in PIT " + object.getUri());
			}
			return null;
		}
		else
			return null;
	}
	
	public void entryTimedOut(Uri uri, int fragmentNumber, String pitIndex) throws LinkBusyException {
		/* pitIndex unused here: simply ignore it! */
		if(pit.existsUri(uri, fragmentNumber))
		{
			pit.removeEntry(uri, fragmentNumber); 		//Just remove the entry in the PIT
			pit.removeTimeoutEvent(uri, fragmentNumber);
		}
		else
		{
			/* Should never happen unless the pit is full */
			//System.err.println(id + ". Should never happen.");
		}
	}
	
	@Override
	protected void printInfo() {
		super.printInfo();
		System.out.println("Remaining memory for packets buffer = " + qSize.getFreeMem());
		System.out.println("PIT timeout events = " + pit.getTimeoutEvents());
	}

	@Override
	public RouterData getRouterData() {
		rData.setPitEntries(pit.getElementsInPIT());
		rData.setUsers(0);
		rData.setPitMemory(pit.getPitSize());
		rData.setFreeQueueDim(qSize.getFreeMem());
		rData.setMaxQueueDim(qSize.getMaxDim());
		return rData;
	}

}
