package it.polito.ccn.simulator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

import org.junit.Assert;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.exception.FullQueueException;
import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.AttackerG;
import it.polito.ccn.simulator.generated.CCNConfigurationG;
import it.polito.ccn.simulator.generated.CCNNodeG;
import it.polito.ccn.simulator.generated.ClientG;
import it.polito.ccn.simulator.generated.LinkG;
import it.polito.ccn.simulator.gui.LinkData;
import it.polito.ccn.simulator.gui.MainGui;
import it.polito.ccn.simulator.gui.PublisherData;
import it.polito.ccn.simulator.gui.RouterData;

public final class CCNNetwork implements Runnable{
	
	public static String pitType;
	
	private final boolean enableGui;
	
	private boolean termSignal = false;
	
	private final List<CCNNode> routers;
	private final List<CCNPublisher> publishers;
	private final List<CCNRegularClient> users;
	private final List<CCNAttacker> attackers;
	private final List<Link> links;
	private final List<Integer> namesLength;
	private final List<Destination> destinationFiles;		// Catalogue
	private final List<Uri> destinations;					// Prefixes in the network
	
	private final double defaultInterestLifeTime;
	private final int packetHeaderSizeInByte;
	private final int clientPipelineSize;
	private final int maxPayloadPerContent;
	private final int networkFiles;
	private final int totalUsers;
	private final int minFileSize = (int) Math.pow(10, 3); 	// in byte
	private final int maxFileSize = (int) Math.pow(10, 6); 	// in byte
	private final int MAX_TIME = 9000;
	private final int DiPIT_REFRESH_INTERVAL = 4;			// Only for the DiPIT
	private final int CHECK_INTERVAL = 200;
	private final long seed;								// Seed for random number generators initialization
	private final Logger logger;
	private final int logLevel;
	private final CCNComponentFactory factory;
	
	private final DecimalFormat dF = new DecimalFormat("#.#########");
	
	private final ZipfDistribution zipf;
	
	private final Logger usersLog;				// Log file for active users
	
	private boolean steady = false; 			// Is the network in the steady state?
	private static double time = 0;
	private double retransmissionRates = 0;
	private double totalDownloadTime = 0;
	private double steadyTime = 0;
	private long totalBytesDownloaded = 0;		// Useful Bytes downloaded from users that completed their job - NET STABLE
	private long tempTotalBytesDownloaded = 0;	// Useful Bytes downloaded from users that completed their job - NET NO STABLE
	private long retransmissions = 0;			// Global retransmissions
	private long usersForRetransmission;		// Measuring mean retransmission rate
	private int usersActive = 0;
	private int tempUsers = 0;
	private int steadyCounter = 0;
	private PriorityQueue<Event> eventsQueue;
	private MainGui f;
	
	/* Inter arrival time */
	private int numberOfArrivalTimes = 0;
	private double arrivalTimesSum = 0;
	
	public CCNNetwork(CCNConfigurationG conf, String attackerUriDim, String routerType, String pitType, String gui)
	{
		CCNNetwork.pitType = pitType;
		if(gui != null && gui.equals("true"))
		{
			enableGui = true;
			try 
			{
				f = new MainGui();
				f.setVisible(true);
			}
			catch(Exception e) { e.printStackTrace(); }
		}
		else
			enableGui = false;
		
		if(conf.getSeed() <= 0)
			seed = (new Random()).nextLong();	// Auto-seeding
		else
			seed = conf.getSeed();
		DecimalFormatSymbols d = DecimalFormatSymbols.getInstance();
		d.setDecimalSeparator('.');
		dF.setDecimalFormatSymbols(d);
		if(enableGui)
		{
			f.printText("Init simulator...");
			f.logOnConsole("Init simulator...");
		}
		totalUsers = conf.getTotalUsers();
		zipf = new ZipfDistribution(seed, conf.getNetworkFiles(), 0.55, 25.0);
		if(attackerUriDim ==  null || routerType == null || pitType == null)
		{
			System.err.println("Network misconfigured. Check the attackerUriDim, routerType, pitType variables to be set.");
			System.err.println("Example usage => java -jar ccnSimulator.jar topology.xml -DattackerUriDim=1000 -DrouterType=Core -DpitType=SimplePIT");
		}
		System.out.println("*******DEVICES CONFIGURATION********");
		System.out.println("AttackerUriDim = " + attackerUriDim);
		System.out.println("RouterType = " + routerType);
		System.out.println("PitType = " + pitType);
		System.out.println("************************************");
		factory 				= new CCNComponentFactory(Integer.parseInt(attackerUriDim), routerType, pitType);
		clientPipelineSize 		= conf.getClientPipelineSize();
		defaultInterestLifeTime = conf.getDefaultInterestLifeTime();
		packetHeaderSizeInByte 	= conf.getPacketHeaderSizeInByte();
		maxPayloadPerContent 	= conf.getMaxPayloadPerContent();
		networkFiles 			= conf.getNetworkFiles();
		logLevel 				= Logger.decodeLogLevel(conf.getLogLevel());
		destinationFiles 		= new ArrayList<Destination>();
		destinations 			= new ArrayList<Uri>();
		namesLength 			= new ArrayList<Integer>();
		logger 					= new Logger(Logger.decodeLogLevel(conf.getLogLevel()), "log.txt");
		usersLog 				= new Logger(Logger.VERBOSE, "logs/users.txt");
		routers 				= new ArrayList<CCNNode>();
		users 					= new ArrayList<CCNRegularClient>();
		links 					= new ArrayList<Link>();
		publishers 				= new ArrayList<CCNPublisher>();
		attackers 				= new ArrayList<CCNAttacker>();
		eventsQueue 			= new PriorityQueue<Event>(1000, new EventComparator());
		Iterator<Integer> it2 = conf.getFilesNamesLengths().getFileNameLength().iterator();
		while(it2.hasNext())
			namesLength.add(new Integer(it2.next()));
		Iterator<LinkG> iter = conf.getLinkG().iterator();
		if(enableGui)
			f.printText("Init links...");
		while(iter.hasNext())
			links.add(new Link(iter.next()));
		Iterator<CCNNodeG> it = conf.getCCNNodeG().iterator();
		if(enableGui)
			f.printText("Init nodes...");
		while(it.hasNext())
		{
			CCNNodeG node = it.next();
			if(node.getCCNRouterG() != null)
				routers.add(factory.getRouter(this, node.getId(), links, node.getCCNRouterG()));
			else if(node.getPublisherG() != null)
			{
				List<Link> list = new ArrayList<Link>();
				list.add(getLinkById(node.getPublisherG().getLink()));
				publishers.add(new CCNPublisher(this, node.getId(), list, node.getPublisherG()));
			}
			else { /* Do nothing */ }
		}
		CCNPublisher p;
		Iterator<CCNPublisher> i = publishers.iterator();
		while(i.hasNext())
		{
			p = i.next();
			// One destination per publisher!
			Iterator<Uri> itUri = p.getDestinations().iterator();
			while(itUri.hasNext())
				destinations.add(itUri.next());
		}
		if(enableGui)
			f.printText("Init destinations...");
		initDestinations();
		if(enableGui)
			f.printText("Init users...");
		it = conf.getCCNNodeG().iterator();
		while(it.hasNext())
		{
			CCNNodeG node = it.next();
			if(node.getAttackerG() != null)
				initAttacker(node.getId(), node.getAttackerG());
			else if(node.getClientG() != null)
				initRegularUser(node.getId(), node.getClientG());
			else { /* Do nothing */ }
		}
		if(enableGui)
			f.printText("Checking constraints...");
		if(!checkNetwork(conf))
			System.out.println("[WARNING] Network is not properly configured.");
		if(enableGui)
			f.printText("Done!");
		Event e = new Event(CHECK_INTERVAL, Event.Type.PERIODIC_CHECK, null, null);
		addEvent(e);
		Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                System.out.println("Terminating simulation.");
                termSignal = true;
            }
        });
	}
	
	public void clearEventsQueue() { eventsQueue.clear(); }
	
	public void logOnConsole(String text) { if(enableGui) f.logOnConsole(text); }
	
	public CCNComponentFactory getFactory() { return factory; }
	
	public void incrementUsersActive() { this.usersActive++; }
	
	public void decrementUsersActive() { this.usersActive--; }
	
	public void incrementRetransmissions() { retransmissions++; }
	
	public ZipfDistribution getZipfDistribution() { return zipf; }
		
	public long getSeed() { return seed; }
	
	public int getDiPITRefreshInterval() { return DiPIT_REFRESH_INTERVAL; }
	
	public List<Integer> getNamesLength() { return namesLength; }
	
	public List<CCNPublisher> getPublishers() { return publishers; }
	
	public List<Destination> getDestinationFiles() { return destinationFiles; }
	
	public List<CCNRegularClient> getUsers() { return users; }
	
	public List<Link> getLinks() { return links; }
	
	public List<CCNNode> getRouters() { return routers; }
	
	public Logger getLogger() {	return logger; }
	
	public int getLogLevel() { return logLevel; }

	public void addEvent(Event e) { eventsQueue.add(e); }
	
	public int getNetworkFilesNumber() { return networkFiles; }
	
	public int getMaxPayloadPerContent() { return maxPayloadPerContent; }
	
	public double getDefaultInterestLifeTime() { return defaultInterestLifeTime; }
	
	public int getPacketHeaderSizeInByte() { return packetHeaderSizeInByte; }
	
	public int getClientPipelineSize() { return clientPipelineSize; }
	
	public static double getTime() { return time; }
	
	public boolean isSteady() { return steady; }
	
	public void manageInterArrivalTime(double interArrivalTime)
	{
		arrivalTimesSum += interArrivalTime;
		numberOfArrivalTimes++;
		return;
	}
	
	public void manageDownloadTermination(ClientStats stats) 
	{
		tempTotalBytesDownloaded += stats.getBytesDownloaded();
		/* Only gather statistics when the network is stable */
		if(!steady)
			return;
		retransmissionRates+=stats.getRetransmissionRate(); 
		usersForRetransmission++;
		totalBytesDownloaded += stats.getBytesDownloaded();
		totalDownloadTime += stats.getDownloadTime();
	}

	@Override
	public void run()
	{
		long events = 0;
		long start = System.currentTimeMillis();
		double temp;
		Event e = eventsQueue.poll();
		if(enableGui)
		{
			f.printText("Running simulation.");
			f.updateTime(time);
			f.updateUsers(usersActive);
		}
		usersLog.log(dF.format(time) + "\t" + usersActive, time, Logger.VERBOSE);
		Iterator<CCNNode> itRouter;
		List<RouterData> v;
		Iterator<Link> itLink;
		List<LinkData> linkV;
		List<PublisherData> publisherL;
		Iterator<CCNPublisher> itPub;
		Link link;
		CCNRouter router;
		/* Main events loop */
		System.out.println("Mean inter arrival time = " + (arrivalTimesSum)/(double)numberOfArrivalTimes);
		try
		{
			while(e != null && time < MAX_TIME && !termSignal)
			{
				if(enableGui && f.isStopped())
					break;
				Assert.assertTrue(e.getTimestamp() >= time);
				time = e.getTimestamp();
				if(!e.isCancelled())
				{
					switch(e.getType())
					{
						case USER_ARRIVAL:
							/* New user arrival == starting a new download */
							Assert.assertTrue(e.getSource() instanceof CCNEdgeRouter);
							/* Only edge routers */
							((CCNEdgeRouter) e.getSource()).addUser();
							break;
						case NEW_INTEREST:
							logger.log("[EVENT] New Interest: "+((CCNInterest)e.getPackt()).getUri()+" ("+e.getPackt().getNonce()+") " + e.getSource(), e.getTimestamp(), Logger.VERBOSE);
							try {
								manageNewInterestEvent(e);
							} catch (LinkBusyException e2) {
								System.err.println("[ERR] Link malfunction. Link was busy.");
								e2.printStackTrace();
							}
							break;
						case PACKET_ARRIVAL:
							logger.log("[EVENT] New packet arrival on link: "+e.getLink().getId()+" ("+e.getPackt().getNonce()+") - Size = " + e.getPackt().getSizeInByte() + " bytes", e.getTimestamp(), Logger.VERBOSE);
							try {
								managePacketArrivalEvent(e);
							} catch (LinkBusyException e2) {
								System.err.println("[ERR] Link malfunction. Link was busy.");
								e2.printStackTrace();
							}
							break;
						case SERVICE_FINISHED:
							logger.log("[EVENT] Service finished at node: "+e.getSource().getId(), e.getTimestamp(), Logger.VERBOSE);
							try {
								manageServiceFinishedEvent(e);
							} catch (LinkBusyException e1) {
								System.err.println("[ERR] Link malfunction. Link was busy.");
								e1.printStackTrace();
							}
							break;
						case TRANSMISSION_FINISHED:
							logger.log("[EVENT] Transmission finished on link: "+e.getLink().getId()+" for packet "+e.getPackt().getNonce() + "  - Size = " + e.getPackt().getSizeInByte() + " bytes", e.getTimestamp(), Logger.VERBOSE);
							manageTransmissionFinishedEvent(e);
							break;
						case SILENCE_FINISHED:
							logger.log("[EVENT] " + e.getSource().getId() + " silence finished", e.getTimestamp(), Logger.VERBOSE);
							manageSilenceFinishedEvent(e);
							break;
						case PIT_ENTRY_TIMEOUT:
							logger.log("[EVENT] Expired PIT entry ("+e.getUri()+")", e.getTimestamp(), Logger.VERBOSE);
							managePitEntryTimeoutEvent(e);
							break;
						case RESEND_INTEREST:
							logger.log("[EVENT] Interest retransmission", e.getTimestamp(), Logger.VERBOSE);
							try {
								retransmissions++;
								manageResendInterestEvent(e);
							} catch (LinkBusyException e1) {
								e1.printStackTrace();
							}
							break;
						case REFRESH_PIT:
							logger.log("[EVENT] PIT refresh", e.getTimestamp(), Logger.VERBOSE);
							Assert.assertTrue(e.getSource() instanceof CCNDiPITRouter);
							((CCNDiPITRouter)e.getSource()).refreshSBF();
							e.setTimestamp(time + DiPIT_REFRESH_INTERVAL);
							addEvent(e);	// Reschedule the event
							break;
						case PERIODIC_CHECK:
							managePeriodicCheck(e);
							break;
						default:
							/* Should never happen */
							System.out.println("Unrecognized event!");
							break;
					}
				}
				events++;
				e = eventsQueue.poll();
				if((events % 500000) == 0)
				{
					/* Update GUI */
					if(enableGui)
					{
						temp = time/MAX_TIME;
						temp*=100;
						f.updateProgress((int)temp);
						f.updateTime(time);
						f.updateUsers(usersActive);
						v = new ArrayList<RouterData>();
						linkV = new ArrayList<LinkData>();
						itRouter = routers.iterator();
						while(itRouter.hasNext())
						{
							router = (CCNRouter) itRouter.next();
							v.add(router.getRouterData());
						}
						f.updateRouterTable(v);
						itLink = links.iterator();
						while(itLink.hasNext())
						{
							link = itLink.next();
							linkV.add(link.getLinkData());
						}
						f.updateLinkTable(linkV);
						publisherL = new ArrayList<PublisherData>();
						itPub = publishers.iterator();
						while(itPub.hasNext())
						{
							publisherL.add(itPub.next().getPublisherData());
						}
						f.updateProviderTable(publisherL);
					}
					usersLog.log(dF.format(time) + "\t" + usersActive, time, Logger.VERBOSE);
				}
			}	/* End while */
		}		/* End try */
		catch(OutOfMemoryError ex) 
		{
			ex.printStackTrace(); 
			System.err.println("[ERR] Fatal error. Memory finished. Aborting...");
			eventsQueue.clear();
			eventsQueue = null;
			destinationFiles.clear();
			destinations.clear();
			attackers.clear();
			System.gc();	// Hint for the garbage collector
		}
		catch(Exception ex) { ex.printStackTrace(); System.err.println("[ERR] Fatal error. Oooops."); }
		try
		{
			/* Print results */
			int unfinished_downloads = 0;
			double unfinished_downloads_retransmission_rate = 0;
			long execute_time = System.currentTimeMillis() - start;
			CCNRegularClient c;
			Iterator<CCNNode> i = routers.iterator();
			while(i.hasNext())
				i.next().terminate();
			Iterator<CCNRegularClient> it = users.iterator();
			while(it.hasNext())
			{
				c = it.next();
				if(steady)
				{
					unfinished_downloads_retransmission_rate+=c.getStats().getRetransmissionRate(); 
					unfinished_downloads++;
				}
				c.terminate();
			}
			Iterator<CCNPublisher> pubIt = publishers.iterator();
			while(pubIt.hasNext())
				pubIt.next().terminate();
			logger.log("***************************************", time, Logger.EVER);
			logger.log("Tempo di esecuzione: " + execute_time/1000 + " sec", time, Logger.EVER);
			logger.log("Tempo simulato: " + time +" sec\n", time, Logger.EVER);
			logger.log("Eventi gestiti: " + events, time, Logger.EVER);
			logger.log("Ritrasmissioni globali: " + retransmissions, time, Logger.EVER);
			System.out.println("***************************************");
			System.out.format("Execution time: %d sec\n", execute_time/1000);
			System.out.format("Simulated time: %.8f sec\n", time);
			System.out.format("Handled events: %d \n", events);
			System.out.format("Global retransmission: %d \n", retransmissions);
			System.out.format("Useful bytes downloaded: %d \n", totalBytesDownloaded);
			System.out.format("Useful bytes downloaded (from the begininning of the simulation): %d \n", tempTotalBytesDownloaded);
			if(unfinished_downloads != 0)
			{
				unfinished_downloads_retransmission_rate /= (double) unfinished_downloads;
				System.out.format("Average percentage of retransmissions for "+ unfinished_downloads +" pending downloads: %.4f \n", unfinished_downloads_retransmission_rate);
				logger.log("Average percentage of retransmissions for "+ unfinished_downloads +" pending downloads: "+ unfinished_downloads_retransmission_rate, time, Logger.EVER);
			}
			if(steady)
			{
				logger.log("Network was stable at " + steadyTime, time, Logger.EVER);
				System.out.format("Network was stable at %.1f\n", steadyTime);
				double throughput = (double)totalBytesDownloaded / (double)time;
				System.out.println("Average Throughput (useful bytes/sec): " + throughput);
			}
			if(usersForRetransmission != 0)
			{
				retransmissionRates /= (double)usersForRetransmission;
				logger.log("Average percentage of retransmissions: " + retransmissionRates, time, Logger.EVER);
				System.out.format("Average percentage of retransmissions: %.4f \n", retransmissionRates);
			}
			else
			{
				logger.log("Average percentage of retransmissions not available: network was not stable :(", time, Logger.EVER);
				System.out.println("Average percentage of retransmissions not available: network was not stable :(");
			}
			Iterator<Link> linkIterator = links.iterator();
			Link l;
			CCNNode n;
			while(linkIterator.hasNext())
			{
				l = linkIterator.next();
				System.out.println("Link " + l.getId());
				i = l.getNodes().iterator();
				while(i.hasNext())
				{
					n = i.next();
					System.out.println("       Node " + n.getId() + " -> Utilization " + l.getLinkUtilization(n.getId()) + "/" + time);
				}
			}
			System.out.println("Average download time = " + (totalDownloadTime / (double)usersForRetransmission));
			logger.log("Average download time = " + (totalDownloadTime / (double)usersForRetransmission), time, Logger.EVER);
			logger.stopAndWait();
			if(enableGui)
				f.dispose();
			if(e == null)
			{
				logger.log("Events queue empty. Simulation is over.", time, Logger.EVER);
				System.out.println("Events queue empty. Simulation is over.");
			}
		} 
		catch (InterruptedException e1) { e1.printStackTrace(); }
	}
	
	private void managePeriodicCheck(Event e)
	{
		final int VARIANCE = 2;
		/* Periodic Chech event rescheduled */
		e.setTimestamp(time + CHECK_INTERVAL);
		addEvent(e);
		/* Check on the users: is anyone stalling? */
		Iterator<CCNRegularClient> usersIterator = users.iterator();
		CCNRegularClient user;
		while(usersIterator.hasNext())
		{
			user = usersIterator.next();
			if(user.isStalling())
				System.err.println("Client " + user.getId() + " probably stalling!");
		}
		/* Reaching steady state is based on active users number */
		if(steady)
			return;
		double variance = usersActive - tempUsers;
		variance = Math.abs((double)variance / (double)usersActive)*100; // variation (in percentage) w.r.t. total number of users
		if(variance <= VARIANCE) // variation is less or equal than VARIANCE%100
			steadyCounter++;
		else
		{
			steadyCounter = 0;
			tempUsers = usersActive;
		}
		if(steadyCounter > 60/CHECK_INTERVAL)
		{
			/* The network is stable */
			steady = true;
			steadyTime = time;
			if(enableGui)
				f.setNetworkStable(true);
		}
		return;
	}
	
	private void manageResendInterestEvent(Event e) throws LinkBusyException
	{
		Assert.assertTrue(e.getRoutable().size() == 1);
		Routable r = e.getRoutable().get(0);
		CCNNode node = e.getSource();
		double transmission_time;
		if(r.getSource().isEmptyOut(r.getLink()) && !r.getLink().isBusy(node.getId()))
		{
			r.getLink().takeLink(node.getId());
			transmission_time = (r.getPacket().getSizeInByte())*8 / (r.getLink().getCapacity(r.getSource().getId()) * Math.pow(10, 6));
			e.setType(Event.Type.TRANSMISSION_FINISHED);
			e.setLink(r.getLink());
			e.setSource(r.getSource());
			e.setPackt(r.getPacket());
			e.setTimestamp(time + transmission_time);
			eventsQueue.add(e);
		}
		else
		{
			/* ...enqueuing the packet. */
			try {
				r.getSource().enqueueOut(r, r.getLink());
			}
			catch (FullQueueException e2) {
				/* Should never happen */
				e2.printStackTrace();
			}
		}
	}
	
	public void managePitEntryTimeoutEvent(Event e) 
	{
		if((e.getSource() instanceof CCNRouter))
		{
			CCNRouter router = (CCNRouter) e.getSource();
			Uri uri = e.getUri();
			try {
				router.entryTimedOut(uri, e.getPackt().getFragmentNumber(), e.getPitIndex());
			} catch (LinkBusyException e1) {
				e1.printStackTrace();
			}
		}
		e = null;
		return;
	}

	public void manageSilenceFinishedEvent(Event e)
	{
		CCNInterest i;
		if(e.getSource() instanceof CCNRegularClient)
		{
			CCNRegularClient user = (CCNRegularClient) e.getSource();
			i = user.nextInterest();
			if(i != null)
			{
				e.setType(Event.Type.NEW_INTEREST);
				e.setPackt(i);
				eventsQueue.add(e);
			}
			else
				System.err.println("Client " + e.getSource().getId() + " stalling!");	// Should never happen!!
		}
	}

	public void manageNewInterestEvent(Event e) throws LinkBusyException 
	{
		CCNInterest interest = (CCNInterest) e.getPackt();
		CCNInterest i;
		CCNNode node = e.getSource();
		Event event;
		Link link = e.getLink();
		double transmission_time = 0;
		/* If queue is empty and the link is free, then I can transmit the packet */
		if(!e.getLink().isBusy(node.getId()) && node.isEmptyOut(e.getLink()))
		{
			transmission_time = (interest.getSizeInByte())*8 / (e.getLink().getCapacity(node.getId()) * Math.pow(10, 6));
			e.getLink().takeLink(node.getId());
			e.setTimestamp(time + transmission_time);
			e.setType(Event.Type.TRANSMISSION_FINISHED);
			e.setSource(node);
			e.setPackt(interest);
			eventsQueue.add(e);
		}
		else
		{
			/* Packet enqueued */
			try {
				e.getSource().enqueueOut(new RoutingResult(e.getPackt(), e.getLink(), e.getSource()), e.getLink());
			} catch (FullQueueException e1) {
				System.err.println("[ERR] Interest sending failed.");
				e1.printStackTrace();
			}
		}
		if(node instanceof CCNRegularClient)
		{
			i = ((CCNRegularClient) node).nextInterest();
			if(i != null)
				eventsQueue.add(new Event(time, Event.Type.NEW_INTEREST, link, node, i));
		}
		if(node instanceof CCNAttacker)
		{
			transmission_time = (interest.getSizeInByte())*8 / (link.getCapacity(node.getId()) * Math.pow(10, 6));
			event = new Event(time + transmission_time, Event.Type.NEW_INTEREST, link, node, ((CCNAttacker) node).nextInterest());
			eventsQueue.add(event);
		}
	}
	
	public void manageTransmissionFinishedEvent(Event e)
	{
		Link link = e.getLink();
		CCNNode node = e.getSource();
		e.setTimestamp(time + e.getLink().getPropagationDelay());
		e.setType(Event.Type.PACKET_ARRIVAL);
		e.setSource(node);
		e.setPackt(e.getPackt());
		eventsQueue.add(e);
		if(node.isEmptyOut(link))
		{
			link.freeLink(node.getId());
			return;
		}
		/* Sending the next enqueued packet */
		Routable r = node.dequeueOut(link);
		Assert.assertTrue(r.getLink().getId().equals(link.getId()));
		Assert.assertTrue(r.getSource().getId().equals(node.getId()));
		double transmission_time = (r.getPacket().getSizeInByte())*8 / (r.getLink().getCapacity(r.getSource().getId()) * Math.pow(10, 6));
		Event event = new Event(time + transmission_time, Event.Type.TRANSMISSION_FINISHED, r.getLink(), r.getSource(), r.getPacket());
		eventsQueue.add(event);
	}

	public void managePacketArrivalEvent(Event e) throws LinkBusyException
	{
		CCNNode node = e.getSource();
		Iterator<CCNNode> i = e.getLink().getNodes().iterator();
		CCNNode destination;
		Routable r;
		Link link = e.getLink();
		CCNPacket packet = e.getPackt();
		while(i.hasNext())
		{
			destination = i.next();
			/* Packet processed from the other end of the link */
			if(!destination.getId().equals(node.getId()))
			{
				/* If CPU is free e we don't have more packets, we then process the packet istantly */
				if(!destination.isBusy(e.getLink()) && destination.isEmptyIn(e.getLink()))
				{
					List<Routable> l = destination.processPacket(e);
					if(l != null)
					{
						/* Schedule the end of the service. */
						if(destination.getServiceTime() == 0)
						{
							e.setTimestamp(time);
							e.setType(Event.Type.SERVICE_FINISHED);
							e.setSource(destination);
							e.setPackt(null);
							e.setRoutable(l);
							manageServiceFinishedEvent(e);
						}
						else
						{
							e.setTimestamp(time + destination.getServiceTime());
							e.setType(Event.Type.SERVICE_FINISHED);
							e.setSource(destination);
							e.setPackt(null);
							e.setRoutable(l);
							eventsQueue.add(e);
						}
						break;
					}
					else
						destination.freeCpu(link);
				}
				else
				{
					/* ...enqueuing the packet. */
					try {
						destination.enqueueIn(new RoutingResult(packet, link, node), link);
						break;
					} catch (FullQueueException e1) {
						System.err.println("[ERR] Queue full at node " + destination.getId() + ". Sender = " + node.getId());
						if(packet instanceof CCNInterest && node instanceof CCNRegularClient)
						{
							CCNInterest interest = (CCNInterest) packet; 
							r = interest.getOrigin().deletePacketFromPipeline();
							Event myEvent = new Event(time + interest.getInterestLifeTime(), Event.Type.RESEND_INTEREST, null, node, null);
							List<Routable> lR = new ArrayList<Routable>();
							lR.add(r);
							myEvent.setRoutable(lR);
							eventsQueue.add(myEvent);
						}
					}
				}
			}
		}
	}
	
	public void manageServiceFinishedEvent(Event e) throws LinkBusyException
	{
		Iterator<Routable> i = e.getRoutable().iterator();
		Routable r, routable;
		CCNNode node = e.getSource();
		double transmission_time;
		Link link = e.getLink();
		while(i.hasNext())
		{
			r = i.next();
			node = r.getSource();
			/* If egress queue is empty and link is free, then I transmit the packet instantly */
			if(node.isEmptyOut(r.getLink()) && !r.getLink().isBusy(node.getId()))
			{
				r.getLink().takeLink(node.getId());
				transmission_time = (r.getPacket().getSizeInByte())*8 / (r.getLink().getCapacity(node.getId()) * Math.pow(10, 6));
				eventsQueue.add(new Event(time + transmission_time, Event.Type.TRANSMISSION_FINISHED, r.getLink(), r.getSource(), r.getPacket()));
			}
			else
			{
				/* ...otherwise I enqueue the packet in the egress queue. */
				try {
					node.enqueueOut(r, r.getLink());
				}
				catch (FullQueueException e1) {
					// Should never happen
					//System.out.println(node.getId() + " sul link " + r.getLink().getId() + ". Pacchetto " + r.getPacket().getUri());
					//e1.printStackTrace();
				}
			}
		}
		if(!node.isEmptyIn(link))
		{
			/* More packets to be processed */
			routable = node.dequeueIn(link);
			e.setTimestamp(time);
			e.setType(Event.Type.PACKET_ARRIVAL);
			e.setSource(routable.getSource());
			e.setLink(routable.getLink());
			e.setPackt(routable.getPacket());
			List<Routable> l = node.processPacket(e);
			if(l != null)
			{
				/* Schedule the end of the service. */
				e.setTimestamp(time + node.getServiceTime());
				e.setType(Event.Type.SERVICE_FINISHED);
				e.setSource(node);
				e.setLink(link);
				e.setPackt(null);
				e.setRoutable(l);
				eventsQueue.add(e);
			}
		}
		else
		{
			node.freeCpu(link);
			e = null;
		}
		return;
	}
	
	/* 
	 * Function invoked at the simulator startup to do more checks about the network.
	 */
	private boolean checkNetwork(CCNConfigurationG conf)
	{
		/* All links must be point to point */
		Iterator<Link> iterator = links.iterator();
		Link l;
		while(iterator.hasNext())
		{
			l = iterator.next();
			if(l.getNodesNumber() > 2)
			{
				/* Fatal error. Only point to point links are supported */
				System.err.println("[ERROR] Every link must contain at most two devices! Link "+l.getId());
				//System.exit(-1);
			}
		}
		/* Check that the network has at least one REACHABLE destination */
		if(destinations.size() < 1)
		{
			System.err.println("[WARNING] The network has no destinations. Set at least one contents' publisher.");
			return false;
		}
		/* The total number of fractions of users must be 1 (the sum) */
		Iterator<CCNNodeG> i = conf.getCCNNodeG().iterator();
		CCNNodeG node;
		ClientG client;
		double sum = 0;
		while(i.hasNext())
		{
			node = i.next();
			client = node.getClientG();
			if(client != null)
				sum += client.getNumberOfClients();
		}
		if(sum != 1)
		{
			System.err.println("[WARNING] Wrong percentages of users. Sum is " + sum);
			return false;
		}
		/* TODO: Add more check (if needed) */
		return true;
	}
	
	public Link getLinkById(String id)
	{
		Iterator<Link> i = links.iterator();
		while(i.hasNext())
		{
			Link l = i.next();
			if(l.getId().equals(id))
				return l;
		}
		return null;
	}
	
	public List<CCNPublisher> getGoodPublishers()
	{
		CCNPublisher p;
		List<CCNPublisher> list = new ArrayList<CCNPublisher>();
		Iterator<CCNPublisher> i = publishers.iterator();
		while(i.hasNext())
		{
			p = i.next();
			if(!p.isAttacker())
				list.add(p);
		}
		return list;
	}
	
	/*
	 * This is executed once, at simulator startup -> don't care about performance! 
	 */
	private void initDestinations()
	{
		int i, j, numBytes, numChunks, lastDataPacketDim;
		Integer length;
		Destination d;
		String s;
		UniformIntegerDistribution dist = new UniformIntegerDistribution(minFileSize, maxFileSize);
		UniformIntegerDistribution dist2 = new UniformIntegerDistribution(97, 122);
		for(i=0; i<networkFiles; i++)
		{
			d = new Destination();
			numBytes = (int) dist.sample();
			d.setNumBytes(numBytes);
			numChunks = (int) (numBytes / maxPayloadPerContent);
			lastDataPacketDim = (int) numBytes % maxPayloadPerContent;
			if(lastDataPacketDim != 0)
				++numChunks;
			d.setNumChunks(numChunks);
			length = namesLength.get(i % namesLength.size()); // length = lunghezza nome risorsa
			char[] array = new char[length];
			int len = array.length;
			for(j=0; j < len; j++)
				array[j] = (char) (dist2.sample());
			s = new String(array);
			d.setName(s);
			CCNPublisher publisher = getGoodPublishers().get(i % getGoodPublishers().size());
			/* Prefix file */
			Uri u = publisher.getDestinations().get(0);
			u.setId(i);
			String[] components = new String[u.getComponents().length];
			System.arraycopy(u.getComponents(), 0, components, 0, u.getComponents().length);
			components[u.getComponents().length - 1] = s;
			try {
				Uri uri = new Uri(components);
				uri.setId(i);
				uri.initArray(d.getNumBytes(), this.maxPayloadPerContent);
				d.setUri(uri);
			} catch (BadUriException e) {
				e.printStackTrace();
			}
			destinationFiles.add(d);
		}
		System.out.println("Files array size = " + destinationFiles.size());
		logger.log("File array size = " + destinationFiles.size(), time, Logger.EVER);
	}

	private void initRegularUser(String id, ClientG client)
	{
		Iterator<Link> iterator = links.iterator();
		while(iterator.hasNext())
		{
			Link l = iterator.next();
			if(l.getId().equals(client.getLink()))
			{
				ArrayList<Link> aList = new ArrayList<Link>();
				aList.add(l);
				CCNEdgeRouter r = new CCNEdgeRouter(this, id+"edgeRouter", aList, (int) (client.getNumberOfClients()*(double)totalUsers), client.getRate());
				routers.add(r);
			}
		}
	}

	/*
	 *                      |Core Router R1|
	 *                            * **
	 *                          *   *  *
	 *                        *     *    *
	 *                      *       *      *
	 *                |Attacker| |EdgeR| |Publisher|
	 *                             ***
	 *                           *  *  * 
	 *                         *    *    *
	 *                      (Cl1) (Cl2) (ClN)			// N clients
	 */
	private void initAttacker(String id, AttackerG attacker) 
	{
		Iterator<Link> iterator = links.iterator();
		CCNAttacker a;
        while (iterator.hasNext()) 
        {
            Link l = iterator.next();
            if (l.getId().equals(attacker.getLink())) 
            {
                ArrayList<Link> aList = new ArrayList<Link>();
                aList.add(l);
				//TODO: Future work: implement more attack types
				/* Currently, only this attack is supported */
				if(attacker.getAttackType().equals("LONG_URI_FLOOD"))
				{
				    a = new CCNAttacker(this, id, aList, Uri.buildStandardUri(attacker.getDestination().getPrefix()), CCNAttacker.Type.LONG_URI_FLOOD, attacker.getInterestLifeTime());
				    eventsQueue.add(new Event(0, Event.Type.NEW_INTEREST, l, a, a.nextInterest()));
				    attackers.add(a);
				}
				else if(attacker.getAttackType().equals("INEXISTENT_URI_FLOOD"))
				{
				    a = new CCNAttacker(this, id, aList, Uri.buildStandardUri(attacker.getDestination().getPrefix()), CCNAttacker.Type.INEXISTENT_URI_FLOOD, attacker.getInterestLifeTime());
				    eventsQueue.add(new Event(0, Event.Type.NEW_INTEREST, l, a, a.nextInterest()));
				    attackers.add(a);
				}
				else // should never happen!
				{
					System.err.println("Unrecognized attack type");
				}
            }
        }
	}

}
