package it.polito.ccn.simulator;

import java.util.Comparator;

public class CachableComparator implements Comparator<Cachable>{

	@Override
	public int compare(Cachable c1, Cachable c2) {
		try{
			if(c1.getTime() < c2.getTime())
				return 1;
			if(c1.getTime() > c2.getTime())
				return -1;
			return 0;
		} 
		catch(Exception ex)
		{
			if(c1 == null)
				return -1;
			if(c2 == null)
				return 1;
			return 0;
		}
	}

}
