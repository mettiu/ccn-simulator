package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

public class HashedPIT extends PIT
{	
	private static final MessageDigest md;
	private final int HASH_OUTPUT = 20;
	
	static
	{
		MessageDigest temp;
		try {
			temp = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Hashing algorithm not available.");
			temp = null;
			e.printStackTrace();
		}
		md = temp;
	}
	
	public HashedPIT(double pitSize)
	{
		super(pitSize);
	}
	
	public int getElementsInPIT() { return pit.size(); }
	
	public boolean existsUri(Uri uri, int fragmentNumber)
	{
		if(pit.containsKey(new String(md.digest(uri.getUriWithFragmentNumber(fragmentNumber).getBytes()))))
			return true;
		else
			return false;
	}
	
	public List<Interface> getInterfacesPending(Uri uri, int fragmentNumber)
	{
		return pit.get(new String(md.digest(uri.getUriWithFragmentNumber(fragmentNumber).getBytes()))).getList();
	}
	
	public boolean addEntry(Uri uri, int fragmentNumber, Interface iFace) throws FullPITException
	{
		String stringKey = uri.getUriWithFragmentNumber(fragmentNumber);
		stringKey = new String(md.digest(stringKey.getBytes()));
		List<Interface> list;
		PITEntry entry = pit.get(stringKey);
		if(entry == null)
		{
			if(pitSize - HASH_OUTPUT >= 0)
			{
				list = new ArrayList<Interface>();
				list.add(iFace);
				entry = new PITEntry();
				entry.setList(list);
				pit.put(stringKey, entry);
				pitSize-=HASH_OUTPUT;
				return true;
			}
			else
				return false;
		}
		else if(entry.getList().contains(iFace))
			return true;
		else
			entry.getList().add(iFace);
		return true;
	}
	
	public void removeEntry(Uri uri, int fragmentNumber)
	{
		String s = new String(md.digest(uri.getUriWithFragmentNumber(fragmentNumber).getBytes()));
		pit.remove(s);
		pitSize+=HASH_OUTPUT;
		Assert.assertTrue(pitSize>=0);
	}

	@Override
	public int getAverageEntrySize() {
		return HASH_OUTPUT;		// SHA-1 hashing algorithm ouput
	}

	@Override
	public void clearAll() {
		pit.clear();
		timeoutEvents.clear();
		pitSize = maxSize;
	}

}
