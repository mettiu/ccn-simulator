package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.CounterOverflowException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;

/* Bloom filter implementation with elements removal (Counting Bloom Filter) */
public class BloomFilter {
	
	private byte[] array;
    private int size;
    private int numberOfAddedElements; 		// number of elements actually added to the Bloom filter
    private int[] res;
    
    public static final int HASHES = 4; 	// number of hash functions
    //private static final Charset charset = Charset.forName("UTF-8"); 	// encoding used for storing hash values as strings
    private static final String hashName = "SHA-1";
    private static final MessageDigest digestFunction;
    
    static { 								// The digest method is reused between instances
        MessageDigest tmp;
        try {
            tmp = java.security.MessageDigest.getInstance(hashName);
        } catch (NoSuchAlgorithmException e) {
            tmp = null;
        }
        digestFunction = tmp;
    }
    
    public BloomFilter(int sizeInByte)
    {
    	size = sizeInByte;
    	array = new byte[sizeInByte];
    	res = new int[HASHES];
    	int i = 0;
    	while(i < array.length)
    		array[i++] = Byte.MIN_VALUE;
    }
    
    public int size() { return numberOfAddedElements; }
    
    public boolean contains(Uri uri, int fragmentNumber) {
        int[] res = uri.getHashes(fragmentNumber);
        int i, j;
        int len = res.length;
        int temp;
        for(i=0; i<res.length; i++)
        	res[i] = Math.abs(res[i]) % size;
        for(i=0; i<len; i++)
        {
        	temp = 1;
        	for(j=0; j<len; j++)
        	{
        		if(i != j && res[j] == res[i])
        			temp++;
        	}
            if (array[res[i]] < Byte.MIN_VALUE + temp) {
                return false;
            }
        }
        return true;
    }
    
    public boolean add(Uri uri, int fragmentNumber) throws CounterOverflowException{
        int[] res = uri.getHashes(fragmentNumber);
        int i;
        for(i=0; i<res.length; i++)
        	res[i] = Math.abs(res[i]) % size;
        int len = res.length;
        int j;
        boolean wasPositive;
        for(i=0; i<len; i++)
        {
        	wasPositive = false;
        	if(array[res[i]] > 0)
        		wasPositive = true;
        	array[res[i]]+=1;
        	if(wasPositive && array[res[i]] < 0)
        	{
        		/* 
        		 * Counter overflow -> Rollback and throw an exception
        		 */
        		for(j=0; j<=i; j++)
        		{
        			array[res[j]]-=1;
        			Assert.assertTrue(array[res[j]] >= Byte.MIN_VALUE);
        		}
        		//throw new CounterOverflowException("Counter Overflow at position " + res[i]);
        		System.err.println(".");
        		return false;
        	}
        }
        numberOfAddedElements++;
        return true;
    }
    
    public void remove(Uri uri, int fragmentNumber) {
    	int[] res = uri.getHashes(fragmentNumber);
    	int i;
    	for(i=0; i<res.length; i++)
    		res[i] = Math.abs(res[i]) % size;
    	Assert.assertEquals(HASHES, res.length);
    	int len = res.length;
        for(i=0; i<len; i++)
        {
        	Assert.assertTrue(array[res[i]] > Byte.MIN_VALUE);
        	//System.out.println("Counter at position " + res[i] + " = " + array[res[i]]);
        	array[res[i]]--;
        }
        numberOfAddedElements--;
    }
    
    /*     (1 - e^(-k * n / m)) ^ k    source=Wikipedia */
    public double getFalsePositiveProbability() 
    {
    	Assert.assertTrue(size != 0);
        return Math.pow((1 - Math.exp(-HASHES * (double) numberOfAddedElements / (double) size)), HASHES);
    }
    
    /* Print Bloom Filter content */
    public void printFilter()
    {
    	int i;
    	System.out.println("***Filter content***");
    	for(i=0; i<array.length; i++)
    		System.out.println("["+i+"]"+" = "+array[i]);
    	System.out.println("********************");
    }
    
    public int[] createHashes(byte[] data) {
        int k = 0;
        byte salt = 0;
        
        while (k < HASHES) {
            byte[] digest;
            digestFunction.update(salt);
            salt++;
            digest = digestFunction.digest(data);

            for (int i = 0; i < digest.length/4 && k < HASHES; i++) {
                int h = 0;
                for (int j = (i*4); j < (i*4)+4; j++) {
                    h <<= 8;
                    h |= ((int) digest[j]) & 0xFF;
                }
                res[k] = h;
                k++;
            }
        }
        /*byte[] b = data;
		int i;
		b = digestFunction.digest(b);
		for(i=0; i<res.length; i++)
		{
			res[i] = Math.abs((b[i*5] << 32) | (b[i*5 +1] << 24) | (b[i*5+2] << 16) | (b[i*5+3] << 8) | b[i*5+4]) % (size-1);
			Assert.assertTrue(res[i] >= 0 && res[i] < size);
			//System.out.println(hashes[i] % size);
		}*/
        return res;
    }
    
    public static double getFalsePositiveProbability(int numberOfElements, double dim) 
    {
        return Math.pow((1 - Math.exp(-HASHES * (double) numberOfElements / dim)), HASHES);
    }
    
    public void reset()
    {
    	int len = array.length, i;
    	for(i=0; i<len; i++)
    		array[i] = Byte.MIN_VALUE;
    }
    
    public int getCounterAt(int index)
    {
    	if(index < 0 || index > size)
    		return -1;
    	return array[index];
    }

}
