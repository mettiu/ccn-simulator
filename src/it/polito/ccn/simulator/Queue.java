package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullQueueException;

import java.util.AbstractQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* ArrayBlocking queue wrapper */
public class Queue {
	
	private AbstractQueue<Routable> queue;
	private QueueSize qSize;
	
	/* dimMax in MB */
	public Queue(QueueSize qSize)
	{
		queue = new LinkedBlockingQueue<Routable>();
		this.qSize = qSize;
	}
	
	public Queue()
	{
		queue = new LinkedBlockingQueue<Routable>();
		/* qSize=0 indica memoria illimitata */
		qSize = new QueueSize(0);
		qSize.setMaxDim(0);
	}
	
	public boolean isEmpty()
	{
		return queue.isEmpty();
	}
	
	public int elementNumber()
	{
		return queue.size();
	}
	
	public void addElement(Routable r) throws FullQueueException
	{
		try
		{
			if(qSize.getMaxDim() == 0)
			{
				queue.offer(r);
				return;
			}
			if(qSize.getFreeMem() > r.getPacket().getSizeInByte())
			{
				queue.offer(r);
				qSize.setFreeMem(qSize.getFreeMem()-r.getPacket().getSizeInByte());
			}
			else
				throw new FullQueueException("Spazio finito -> Coda piena");
		}
		catch(IllegalStateException ex)
		{
			throw new FullQueueException("Illegal state Exception -> Coda piena");
		}
	}
	
	public Routable getElement() throws InterruptedException
	{
		if(isEmpty())
			return null;
		Routable r = queue.remove();
		qSize.setFreeMem(qSize.getFreeMem()+r.getPacket().getSizeInByte());
		if(qSize.getFreeMem() > qSize.getMaxDim())
			qSize.setFreeMem(qSize.getMaxDim());
		return r;
	}

}
