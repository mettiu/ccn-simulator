package it.polito.ccn.simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.LinkG;
import it.polito.ccn.simulator.gui.LinkData;

public class Link 
{	
	private String id;
	private double propagationDelay;
	private List<CCNNode> nodes;
	private Map<String, Boolean> uploadLinkBusy;
	private Map<String, Double> linkBand;
	private Map<String, Double> linkUtilization;
	private Map<String, Double> linkRequestTime;
	private boolean simmetricBand = false;
	private double capacity;
	private LinkData linkData;

	public Link(LinkG link)
	{
		this.id = link.getId();
		this.propagationDelay = link.getDelay();
		linkData = new LinkData();
		nodes = new ArrayList<CCNNode>();
		linkBand = new HashMap<String, Double>();
		uploadLinkBusy = new HashMap<String, Boolean>();
		linkUtilization = new HashMap<String, Double>();
		linkRequestTime = new HashMap<String, Double>();
		if(link.getNode1() == null || link.getNode2() == null)
		{
			if(link.getDown() != link.getUp())
			{
				System.err.println("Error on link "+id+". Downlink and uplink must have the same band. Link capacity set to " + link.getDown());
			}
			simmetricBand = true;
			capacity = link.getDown();
		}
		else
		{
			linkBand.put(link.getNode1(), link.getUp());
			linkBand.put(link.getNode2(), link.getDown());
		}
	}
	
	public double getCapacity(String id)
	{
		if(simmetricBand)
			return capacity;
		if(linkBand.containsKey(id))
			return linkBand.get(id);
		else
			return 0;
	}
	
	public void freeLink(String id)
	{
		if(uploadLinkBusy.containsKey(id))
		{
			if(uploadLinkBusy.get(id) == false)
				System.err.println("Double free!");
			uploadLinkBusy.remove(id);
			uploadLinkBusy.put(id, false);
			
			double d = linkRequestTime.get(id);
			d = CCNNetwork.getTime() - d; // Link utilization time
			double temp = linkUtilization.get(id);
			temp += d;
			linkUtilization.put(id, temp);
		}
	}
	
	public void takeLink(String id) throws LinkBusyException
	{
		if(uploadLinkBusy.containsKey(id))	
		{
			if(uploadLinkBusy.get(id))
				throw new LinkBusyException("Link busy. Cannot get link.");
			uploadLinkBusy.remove(id);
			uploadLinkBusy.put(id, true);
			linkRequestTime.put(id, CCNNetwork.getTime());
		}
		else
			System.err.println("Link not working properly: " + id);
	}
	
	public boolean isBusy(String id)
	{
		if(uploadLinkBusy.containsKey(id))	
			return uploadLinkBusy.get(id);
		return false;
	}
	
	public void addNode(CCNNode node)
	{
		nodes.add(node);
		if(!uploadLinkBusy.containsKey(id))
			uploadLinkBusy.put(node.getId(), false);
		if(!linkUtilization.containsKey(id))
			linkUtilization.put(node.getId(), new Double(0));
	}
	
	public LinkData getLinkData()
	{
		linkData.setLinkId(id);
		Iterator<CCNNode> i = nodes.iterator();
		CCNNode node;
		if(!i.hasNext())
			return linkData;
		node = i.next();
		if(node != null)
		{
			linkData.setNode1(node.getId());
			linkData.setNode1LinkStatus(uploadLinkBusy.get(node.getId()));
		}
		if(!i.hasNext())
			return linkData;
		node = i.next();
		if(node != null)
		{
			linkData.setNode2(node.getId());
			linkData.setNode2LinkStatus(uploadLinkBusy.get(node.getId()));
		}
		return linkData;
	}
	
	public String getId() { return id; }

	public double getPropagationDelay() { return propagationDelay; }
	
	public List<CCNNode> getNodes() { return nodes; }
	
	public int getNodesNumber() { return nodes.size(); }
	
	public double getLinkUtilization(String id) { return linkUtilization.get(id); }

}
