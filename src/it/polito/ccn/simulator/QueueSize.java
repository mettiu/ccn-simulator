package it.polito.ccn.simulator;

public class QueueSize 
{	
	private int maxDim;
	private int freeMem;
	
	public QueueSize(int maxDim)
	{
		this.maxDim = maxDim;
		this.freeMem = maxDim;
	}
	
	public int getMaxDim()
	{
		return maxDim;
	}
	
	public int getFreeMem()
	{
		return freeMem;
	}
	
	public void setFreeMem(int freeMem)
	{
		this.freeMem = freeMem;
	}
	
	public void setMaxDim(int maxDim)
	{
		this.maxDim = maxDim;
	}

}
