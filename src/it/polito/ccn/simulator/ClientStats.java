package it.polito.ccn.simulator;

public class ClientStats {
	
	private int numChunks;
	private int realChunks;
	private double startTime;
	private double endTime;
	private int bytesDownloaded;
	
	public ClientStats(int numChunks, int realChunks, double startTime, double endTime, int bytesDownloaded)
	{
		this.numChunks = numChunks;
		this.realChunks = realChunks;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bytesDownloaded = bytesDownloaded;
	}
	
	public double getRetransmissionRate()
	{
		double retransmissionRate = ((double)(numChunks - realChunks) / (double)numChunks) * 100;
		return retransmissionRate;
	}
	
	public double getDownloadTime()
	{
		return (endTime - startTime);
	}
	
	public int getBytesDownloaded()
	{
		return bytesDownloaded;
	}

}
