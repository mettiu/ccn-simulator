package it.polito.ccn.simulator;

public class RoutingResult implements Routable {
	
	private CCNPacket packet;
	private Link link;
	private CCNNode source;
	
	public RoutingResult(CCNPacket packet, Link link, CCNNode source)
	{
		this.packet = packet;
		this.link = link;
		this.source = source;
	}

	@Override
	public CCNPacket getPacket() {
		return packet;
	}

	@Override
	public Link getLink() {
		return link;
	}

	@Override
	public CCNNode getSource() {
		return source;
	}

}
