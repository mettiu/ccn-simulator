package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.CounterOverflowException;
import it.polito.ccn.simulator.exception.FullPITException;

import java.util.List;

/*
 * DiPIT with bloom filter implementation
 */
public class DiPITBF extends PIT{
	
	private BloomFilter bf;
	private int pitElements;

	public DiPITBF(double pitSize) {
		super(pitSize);
		pitElements = 0;
		bf = new BloomFilter((int)(pitSize * Math.pow(10, 6)));
	}

	@Override
	public int getAverageEntrySize() {
		return 0;
	}

	@Override
	public List<Interface> getInterfacesPending(Uri uri, int fragmentNumber) {
		return null; 
	}

	@Override
	public boolean addEntry(Uri uri, int fragmentNumber, Interface iFace) throws FullPITException {
		try{
			boolean res = bf.add(uri, fragmentNumber);
			if(res)
				pitElements++;
			return res;
		} catch (CounterOverflowException e) { throw new FullPITException("Counter Overflow!"); }
	}

	@Override
	public void removeEntry(Uri uri, int fragmentNumber) {
		bf.remove(uri, fragmentNumber);
		pitElements--;
	}
	
	@Override
	public boolean existsUri(Uri uri, int fragmentNumber) {
		if(bf.contains(uri, fragmentNumber))
			return true;
		return false;
	}

	@Override
	public void clearAll() {
		bf.reset();
	}
	
	@Override
	public int getElementsInPIT() {
		return pitElements;
	}

}
