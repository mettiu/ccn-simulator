//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.08 at 11:08:35 AM CEST 
//


package it.polito.ccn.simulator.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CCNNodeG complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CCNNodeG">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CCNRouterG" type="{http://www.example.org/NetworkSchema}CCNRouterG"/>
 *           &lt;element name="PublisherG" type="{http://www.example.org/NetworkSchema}PublisherG"/>
 *           &lt;element name="AttackerG" type="{http://www.example.org/NetworkSchema}AttackerG"/>
 *           &lt;element name="ClientG" type="{http://www.example.org/NetworkSchema}ClientG"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CCNNodeG", propOrder = {
    "ccnRouterG",
    "publisherG",
    "attackerG",
    "clientG"
})
public class CCNNodeG {

    @XmlElement(name = "CCNRouterG")
    protected CCNRouterG ccnRouterG;
    @XmlElement(name = "PublisherG")
    protected PublisherG publisherG;
    @XmlElement(name = "AttackerG")
    protected AttackerG attackerG;
    @XmlElement(name = "ClientG")
    protected ClientG clientG;
    @XmlAttribute(name = "id", required = true)
    protected String id;

    /**
     * Gets the value of the ccnRouterG property.
     * 
     * @return
     *     possible object is
     *     {@link CCNRouterG }
     *     
     */
    public CCNRouterG getCCNRouterG() {
        return ccnRouterG;
    }

    /**
     * Sets the value of the ccnRouterG property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCNRouterG }
     *     
     */
    public void setCCNRouterG(CCNRouterG value) {
        this.ccnRouterG = value;
    }

    /**
     * Gets the value of the publisherG property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherG }
     *     
     */
    public PublisherG getPublisherG() {
        return publisherG;
    }

    /**
     * Sets the value of the publisherG property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherG }
     *     
     */
    public void setPublisherG(PublisherG value) {
        this.publisherG = value;
    }

    /**
     * Gets the value of the attackerG property.
     * 
     * @return
     *     possible object is
     *     {@link AttackerG }
     *     
     */
    public AttackerG getAttackerG() {
        return attackerG;
    }

    /**
     * Sets the value of the attackerG property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttackerG }
     *     
     */
    public void setAttackerG(AttackerG value) {
        this.attackerG = value;
    }

    /**
     * Gets the value of the clientG property.
     * 
     * @return
     *     possible object is
     *     {@link ClientG }
     *     
     */
    public ClientG getClientG() {
        return clientG;
    }

    /**
     * Sets the value of the clientG property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientG }
     *     
     */
    public void setClientG(ClientG value) {
        this.clientG = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
