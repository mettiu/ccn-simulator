package it.polito.ccn.simulator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UnknownFormatConversionException;
import java.util.concurrent.LinkedBlockingQueue;

public class LoggerThread extends Thread{
	
	private LinkedBlockingQueue<String> messagesQueue;
	private PrintWriter pWriter;
	private boolean stop = false;
	
	public LoggerThread(LinkedBlockingQueue<String> messagesQueue, String path)
	{
		this.messagesQueue = messagesQueue;
		try {
			/* Open log file in append mode */
			File f = new File(path);
			if(f.exists())
				f.delete();
			f.createNewFile();
			pWriter = new PrintWriter(new FileWriter(f), true);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Unable to open log file in writing mode");
			pWriter = null;
		}
	}
	
	@Override
	public void run() {
		String msg = null;
		while(!stop)
		{
			try {
				msg = messagesQueue.take();
				if(pWriter!=null)
					pWriter.format(msg + "%n");
				else
					System.out.println("Unable to write on log file");
			} catch (InterruptedException e) {
				stop = true;
			} catch (UnknownFormatConversionException ex) {
				System.out.println("Error logging \"" + msg + "\"");
			}
		}
		/* Finisco il log dei messaggi in coda */
		while(!messagesQueue.isEmpty())
		{
			try {
				msg = messagesQueue.poll();
				if(pWriter!=null)
					pWriter.format(msg + "%n");
				else
					System.out.println("Unable to write on log file");
			} catch (UnknownFormatConversionException ex) {
				System.out.println("Error logging \"" + msg + "\"");
			}
		}
		pWriter.append("Done!");
		pWriter.close();
	}
	
	public void stopLog()
	{
		stop = true;
	}

}
