package it.polito.ccn.simulator;

public class CCNContentObject extends CCNPacket{
	
	private int contentSizeInByte;
	private boolean lastChunk;
	private CCNInterest interest;
	
	public CCNContentObject(Uri name, int fragmentNumber, int headerSizeInByte, int contentSizeInByte, boolean lastChunk, CCNInterest interest)
	{
		super(name, fragmentNumber, headerSizeInByte);
		this.contentSizeInByte = contentSizeInByte;
		this.lastChunk = lastChunk;
		this.setInterest(interest);
	}
	
	public boolean isLastChunk() { return lastChunk; }

	@Override
	public int getSizeInByte() { return headerSizeInByte + uri.getUriSizeInByte() + contentSizeInByte; }

	public CCNInterest getInterest() { return interest; }

	public final void setInterest(CCNInterest interest) { this.interest = interest; }

}
