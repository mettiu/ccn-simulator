package it.polito.ccn.simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import it.polito.ccn.simulator.exception.FullQueueException;

public abstract class CCNNode implements PacketProcessor{
	
	protected List<Link> links;
	protected HashMap<String, Boolean> cpuBusy;
	protected HashMap<String, Queue> ingressQueues;
	protected HashMap<String, Queue> egressQueues;
	protected String id;
	protected CCNNetwork network;
	protected double serviceTime;
	
	public CCNNode(CCNNetwork network, String id, List<Link> links)
	{
		assert links.get(0) != null;
		this.id = id;
		this.network = network;
		this.links = new ArrayList<Link>();
		this.links.add(links.get(0));
		this.serviceTime = 0;
		ingressQueues = new HashMap<String, Queue>();
		egressQueues = new HashMap<String, Queue>(); 
		cpuBusy = new HashMap<String, Boolean>();
	}
	
	public abstract List<Routable> processPacket(Event e);
	
	public abstract void terminate();
	
	public Routable deletePacketFromPipeline() { return null; }
	
	public String getId() { return id; }
	
	public double getServiceTime() { return serviceTime; }
	
	public boolean isBusy(Link link) {
		Boolean result = cpuBusy.get(link.getId());
		if(result == null)
			return false;
		else 
			return result;
	}
	
	public List<Link> getLinks() { return links; }
	
	public void enqueueIn(Routable r, Link link) throws FullQueueException {
		ingressQueues.get(link.getId()).addElement(r);
	}

	public int getInQueueSize(Link link) {
		return ingressQueues.get(link.getId()).elementNumber();
	}

	public int getOutQueueSize(Link link) {
		return egressQueues.get(link.getId()).elementNumber();
	}

	public Routable dequeueIn(Link link) {
		try {
			return ingressQueues.get(link.getId()).getElement();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void enqueueOut(Routable r, Link link) throws FullQueueException {
		egressQueues.get(link.getId()).addElement(r);		
	}

	public Routable dequeueOut(Link link) {
		try {
			return egressQueues.get(link.getId()).getElement();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean isEmptyIn(Link link) {
		if(link == null)
			System.err.println("Nodo "+id+" Link null");
		return ingressQueues.get(link.getId()).isEmpty();
	}

	public boolean isEmptyOut(Link link) {
		if(link == null)
			System.err.println("Nodo "+id+" Link null");
		return egressQueues.get(link.getId()).isEmpty();
	}
	
	public void freeCpu(Link link)
	{
		cpuBusy.put(link.getId(), new Boolean(false));
		return;
	}
	
	public int queueOutSize()
	{
		Iterator<Queue> it = egressQueues.values().iterator();
		int res = 0;
		while(it.hasNext())
		{
			res += it.next().elementNumber();
		}
		return res;
	}
	
	public int queueInSize()
	{
		Iterator<Queue> it = ingressQueues.values().iterator();
		int res = 0;
		while(it.hasNext())
		{
			res += it.next().elementNumber();
		}
		return res;
	}

}
