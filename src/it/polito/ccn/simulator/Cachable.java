package it.polito.ccn.simulator;

public class Cachable {
	
	private Uri uri;
	private double time;
	private int dim;
	
	public Cachable(Uri uri, double time, int dim)
	{
		this.uri = uri;
		this.time = time;
		this.dim = dim;
	}
	
	public Uri getUri() { return uri; }
	
	public double getTime() { return time; }
	
	public int getDim() { return dim; }

}
