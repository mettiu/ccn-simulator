package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.BadUriException;

import java.util.List;
import java.util.UUID;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

public class CCNAttacker extends CCNNode{
	
	public static enum Type {
		LONG_URI_FLOOD, INEXISTENT_URI_FLOOD;
	}
	
	/* Percentage of Interest with URI with MAX bytes size */
	private final int MAX_URI_RATE = 100;
	
	private Type type;
	private Uri destination;
	private double interestLifeTime;
	private CCNInterest interest;
	private String[] components;
	private Link link;
	private UniformIntegerDistribution dist;
	
	public CCNAttacker(CCNNetwork network, String id, List<Link> links, Uri destination, Type type, double interestLifetime) {
		super(network, id, links);
		assert links.size() == 1;
		this.link = links.get(0);
		this.destination = destination;
		this.type = type;
		this.interestLifeTime = interestLifetime;
		components = new String[destination.getComponents().length + 1];
		System.arraycopy(destination.getComponents(), 0, components, 0, destination.getComponents().length);
		components[components.length-2] = "a";
		/* Set the fragment of the resource we want to retrieve */
		components[components.length-1] = "s0";
		link.addNode(this);
		ingressQueues.put(link.getId(), new Queue());
		egressQueues.put(link.getId(), new Queue());
		dist = new UniformIntegerDistribution(1, 100);
		System.out.println("Attacker " + id + ". Interest Lifetime = " + interestLifetime + " sec");
	}
	
	public Link getLink() { return link; }
	
	public Uri getDestination() { return destination; }
	
	public String getAttackType() { return type.toString(); }

	@Override
	public List<Routable> processPacket(Event e) {
		/* Should never be called unless DiPIT is used */
		//Assert.assertTrue(1 == 2);
		return null;
	}

	@Override
	public double getServiceTime() {
		return 0.0;
	}

	public double getInterestLifeTime() {
		return interestLifeTime;
	}
	
	public CCNInterest nextInterest()
	{
		try 
		{
			Uri uri = new Uri(components);
			int sample = dist.sample();
			if(sample <= MAX_URI_RATE)
				uri.setUriSizeInByte(network.getFactory().getAttackerUriDim() + 13);
			else
				uri.setUriSizeInByte(20);
			uri.setId(Math.abs(UUID.randomUUID().hashCode()));
			uri.initArray(network.getMaxPayloadPerContent(), network.getMaxPayloadPerContent());
			interest = new CCNInterest(uri, 0, interestLifeTime, network.getPacketHeaderSizeInByte(), 0, this);
			//System.out.println("Attacker Interest size = " + interest.getSizeInByte() + " bytes");
		}
		catch (BadUriException e) { e.printStackTrace(); }
		return interest;
	}

	@Override
	public void terminate() {
		/* Nothing to do */
	}

}
