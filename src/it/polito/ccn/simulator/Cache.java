package it.polito.ccn.simulator;

import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Enumeration;

/**
 * Simple implementation of a cache, using Least Recently Used algorithm
 * for discarding members when the cache fills up
 */
public class Cache {
    
	/** The cache */
    private Hashtable<String, Cachable> cache = new Hashtable<String, Cachable>();
    /** The linked list to keep track of least/most recently used */
    private LinkedList<String> lru = new LinkedList<String>();
    /** The maximum size of the cache */
    private int maxSize;
    private boolean active;
    private double totalSearches;
	private double hits;
	private int currentMem;

    /**
     * Constructor
     */
    public Cache(CCNNetwork network, final int maxSize) {
        this.maxSize = (int)(maxSize * Math.pow(10, 6))/8;
        this.currentMem = this.maxSize;
		this.active = true;
		totalSearches = 0;
		hits = 0;
    }
    
    public Cache()
    {
    	this.maxSize = 0;
		this.active = false;
    }

    public int LRUSize() {
    	return lru.size();
    }
    
    public int cacheSize() {
        return cache.size();
    }

    /**
     * Puts a new object in the cache.  If the cache is full, it removes
     * the least recently used object.  The new object becomes the most
     * recently used object.
     */
    public void addElement(Uri key, int fragmentNumber, int value) {
    	if(!active)
			return;
        List<Cachable> removed = new ArrayList<Cachable>();
        String stringKey = key.getUriWithFragmentNumber(fragmentNumber);
        // make room if needed
        while (currentMem - value < 0) {
            removed.add(removeLRU());
        }
        //remove the key from the list if it's in the cache already
      	Cachable cacheValue = cache.get(stringKey);
      	if (cacheValue != null) {
      		if (cacheValue.getDim() != value)
      			removed.add(cacheValue);
      		lru.remove(stringKey);
      	}
        // the last item in the list is the most recently used
        lru.addLast(stringKey);
        // put it in the actual cache
        cache.put(stringKey, new Cachable(key, CCNNetwork.getTime(), value));
        cleanupAll(removed);
        currentMem-=value;
    }

    /**
     * Gets an object from the cache.  This object is set to be the
     * most recenty used
     */
    public int searchObject(Uri key, int fragmentNumber) {
    	if(!active)
			return 0;
    	String stringKey = key.getUriWithFragmentNumber(fragmentNumber);
    	totalSearches++;
        // check for existence in cache
        if (!cache.containsKey(stringKey)) {
            return 0;
        }
        // set to most recently used
        hits++;
        lru.remove(stringKey);
        lru.addLast(stringKey);
        // return the object
        return cache.get(stringKey).getDim();
    }

    /**
     * Removes the object from the cache and the lru list
     */
    public Cachable remove(String key) {
        // check for existence in cache
        if (!cache.containsKey(key.toString())) {
            return null;
        }
        // remove from lru list
        lru.remove(key.toString());
        // remove from cache
        Cachable obj = cache.remove(key.toString());
        currentMem+=obj.getDim();
        return obj;
    }

    private Cachable removeLRU() {
        Cachable obj = cache.remove(lru.getFirst());
        lru.removeFirst();
        currentMem+=obj.getDim();
        return obj;
    }

    /**
     * Resize the cache
     */
    public void resize(int newSize) {
        if (newSize <= 0) {
            return;
        }
        List<Cachable> removed = new ArrayList<Cachable>();
        maxSize = newSize;
        while (cache.size() > maxSize) {
            removed.add(removeLRU());
        }
        cleanupAll(removed);
    }

    private void cleanupAll(List<Cachable> removed) {
        Iterator<Cachable> it = removed.iterator();
        while (it.hasNext()) {
            cleanupObject(it.next());
        }
    }
    
    /**
     * Override this method to do special cleanup on an object,
     * such as closing a statement or a connection
     */
    protected void cleanupObject(Object o) {}

    public void cleanupAll() {
        for (Enumeration<String> enumeration = cache.keys(); enumeration.hasMoreElements();) {
            Cachable o = remove(enumeration.nextElement());
            cleanupObject(o);
        }
    }
    
    public void printInfo()
	{
    	if(!active)
    	{
    		System.out.println("Cache disabled");
    		return;
    	}
		if(totalSearches == 0)
			return;
		double temp = hits / totalSearches;
		temp *= 100;
		DecimalFormat dF = new DecimalFormat("#.##");
		System.out.println("Hit percentage on the cache = " + dF.format(temp) + " %");
	}
  
}
