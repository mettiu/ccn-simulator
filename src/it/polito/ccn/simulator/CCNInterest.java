package it.polito.ccn.simulator;

public final class CCNInterest extends CCNPacket{
	
	private Integer minSuffixComponents;
	private Integer maxSuffixComponents;
	private double interestLifeTime;	// in seconds
	private int responseContentDim;
	private boolean isLastChunk;
	private CCNNode origin;
	
	public CCNInterest(Uri uri, int fragmentNumber, double interestLifeTime, int minSuffixComponents, int maxSuffixComponents, int headerSizeInByte, int responseContentDim, CCNNode origin)
	{
		super(uri, fragmentNumber, headerSizeInByte);
		this.isLastChunk = false;
		this.minSuffixComponents = minSuffixComponents;
		this.maxSuffixComponents = maxSuffixComponents;
		this.interestLifeTime = interestLifeTime;
		this.headerSizeInByte = headerSizeInByte;
		this.setResponseContentDim(responseContentDim);
		this.origin = origin;
	}
	
	public CCNInterest(Uri uri, int fragmentNumber, double interestLifeTime, int headerSizeInByte, int responseContentDim, CCNNode origin)
	{
		super(uri, fragmentNumber, headerSizeInByte);
		this.isLastChunk = false;
		this.minSuffixComponents = 0;
		this.maxSuffixComponents = Integer.MAX_VALUE;
		this.uri = uri;
		this.interestLifeTime = interestLifeTime;
		this.headerSizeInByte = headerSizeInByte;
		this.setResponseContentDim(responseContentDim);
		this.origin = origin;
	}
	
	public CCNNode getOrigin() { return origin; }
	
	public void setIsLastChunk(boolean isLastChunk) { this.isLastChunk = isLastChunk; }
	
	public boolean isLastChunk() { return isLastChunk; }
	
	public int minSuffixComponents()
	{
		return minSuffixComponents;
	}
	
	public int maxSuffixComponents()
	{
		return maxSuffixComponents;
	}

	public int getNonce() {
		return nonce;
	}
	
	public double getInterestLifeTime()
	{
		return interestLifeTime;
	}
	
	public void setInterestLifeTime(double lifetime)
	{
		this.interestLifeTime = lifetime;
	}
	
	public int getHeaderSizeInByte()
	{
		return headerSizeInByte;
	}

	@Override
	public int getSizeInByte() {
		return headerSizeInByte + uri.getUriSizeInByte();
	}

	public int getResponseContentDim() {
		return responseContentDim;
	}

	public void setResponseContentDim(int responseContentDim) {
		this.responseContentDim = responseContentDim;
	}

}
