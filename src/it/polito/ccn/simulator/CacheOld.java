package it.polito.ccn.simulator;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;

public class CacheOld {
	
	private final int maxDim;
	private int currentMem;
	private double totalSearches;
	private double hits;
	private LinkedHashMap<String, Cachable> cache;
	private boolean active;
	
	/* maxDim in Megabit */
	public CacheOld(CCNNetwork network, final int maxDim)
	{
		this.maxDim = (int)(maxDim * Math.pow(10, 6))/8;
		this.currentMem = this.maxDim;
		this.active = true;
		cache = new LinkedHashMap<String, Cachable>(maxDim, .75F, true) {
			private static final long serialVersionUID = 1L;

			// This method is called just after a new entry has been added
		    @SuppressWarnings("rawtypes")
			public boolean removeEldestEntry(Map.Entry eldest) {
		        return size() > maxDim;
		    }
		};
		totalSearches = 0;
		hits = 0;
	}
	
	public CacheOld()
	{
		this.maxDim = 0;
		this.active = false;
	}
	
	public int getFreeBytes() { return currentMem; }
	
	public int getElementNumber() { return cache.size(); }
	
	public boolean addElement(Uri name, Integer dim)
	{
		if(!active)
			return true;
		/* LRU policy for the cache */
		cache.put(name.toString(), new Cachable(name, CCNNetwork.getTime(), dim));
		return true;
	}
	
	public Integer searchObject(Uri object)
	{
		if(!active)
			return 0;
		totalSearches++;
		Cachable c = cache.get(object.toString());
		if (c == null) 
			return 0;
		else
		{
			/* CACHE HIT! */
			hits++;
			return c.getDim();
		}
	}
	
	public void printInfo()
	{
		if(totalSearches == 0)
			return;
		double temp = hits / totalSearches;
		temp *= 100;
		DecimalFormat dF = new DecimalFormat("#.##");
		System.out.println("Hit percentage on the cache = " + dF.format(temp) + " %");
	}

}
