package it.polito.ccn.simulator;

import it.polito.ccn.simulator.exception.FullPITException;
import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.CCNRouterG;
import it.polito.ccn.simulator.generated.FibG.FibEntry;
import it.polito.ccn.simulator.generated.InterfacesG.InterfaceG;
import it.polito.ccn.simulator.gui.RouterData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

public class CCNDiPITRouter extends CCNRouter
{	
	private Map<String, PIT> pits;
	private int tempElements = -50;

	public CCNDiPITRouter(CCNNetwork network, String id, List<Link> links, CCNRouterG ccnRouter) {
		super(network, id, links);
		routerG = ccnRouter;
		serviceTime = ccnRouter.getServiceTime();
		fib = new FIB();
		pits = new HashMap<String, PIT>();
		cache = new Cache(network, ccnRouter.getCacheMemory());
		/* Maximum queue size conversion from MB in Byte */
		qSize = new QueueSize((int)(ccnRouter.getPacketsQueueMemory()*Math.pow(10, 6)));
		/* Populate the router list interface */
		Iterator<InterfaceG> iter = ccnRouter.getInterfacesG().getInterfaceG().iterator();
		while(iter.hasNext())
		{
			InterfaceG iface = iter.next();
			Interface i = new Interface(iface);
			Link l = getLinkFromId(links, iface.getLink());
			ingressQueues.put(l.getId(), new Queue(qSize));
			egressQueues.put(l.getId(), new Queue(qSize));
			/* One pit for each interface */
			PIT p = network.getFactory().getPIT((double)ccnRouter.getPitMemory()/((double)ccnRouter.getInterfacesG().getInterfaceG().size()+1));
			p.setiFace(i);
			pits.put(i.getId(), p);
			l.addNode(this);
			i.setLink(l);
			interfaceList.add(i);
			network.getLogger().log(id + " - PITi on interface "+i.getId()+" (link "+i.getLink().getId()+") with size (MB) = "+dF.format((double)ccnRouter.getPitMemory()/((double)ccnRouter.getInterfacesG().getInterfaceG().size()+1)), 0, Logger.EVER);
		}
		/* Shared bloom filter */
		pit = network.getFactory().getPIT((double)ccnRouter.getPitMemory()/((double)ccnRouter.getInterfacesG().getInterfaceG().size()+1));
		/* Implementation of the RST mechanism (see DiPIT paper pag. 4) */
		Event e = new Event(CCNNetwork.getTime() + network.getDiPITRefreshInterval(), Event.Type.REFRESH_PIT, this, null);
		network.addEvent(e);
		/* Populate router FIB */
		Iterator<FibEntry> it = ccnRouter.getFibG().getFibEntry().iterator();
		while(it.hasNext())
		{
			FibEntry entry = it.next();
			Uri uri = Uri.buildStandardUri(entry.getPrefix());
			Iterator<Integer> i = entry.getInterface().iterator();
			while(i.hasNext())
				fib.addRoute(uri, getInterfaceFromId(String.valueOf(i.next())));
		}
	}
	
	@Override
	public List<Routable> processPacket(Event e) {
		cpuBusy.put(e.getLink().getId(), true);
		Iterator<Interface> it = interfaceList.iterator();
		Interface i = null;
		/* Look for the arrival interface */
		while(it.hasNext())
		{
			i = it.next();
			if(i.getLink().getId().equals(e.getLink().getId()))
				break;
		}
		Assert.assertNotNull(i);
		int pitElements = getPitElements();
		if(pitElements > tempElements+40 || pitElements < tempElements-40)
		{
			logger.log(dF.format(CCNNetwork.getTime()) + "\t" + pitElements + "\t" + -1, CCNNetwork.getTime(), Logger.VERBOSE);
			tempElements = pitElements;
		}
		if(e.getPackt() instanceof CCNInterest)
		{
			CCNInterest interest = (CCNInterest) e.getPackt();
			// TODO: LifeTime handling code here
			if(interest.isCachable())
			{
				int result = cache.searchObject(interest.getUri(), interest.getFragmentNumber());
				if(result > 0)
				{
					/* Not found in cache */
					CCNContentObject object = new CCNContentObject(interest.getUri(), interest.getFragmentNumber(), network.getPacketHeaderSizeInByte(), result, interest.isLastChunk(), interest);
					RoutingResult rR = new RoutingResult(object, e.getLink(), this);
					List<Routable> l = new ArrayList<Routable>();
					l.add(rR);
					return l;
				}
			}
			/* Get the PIT corresponding to this interface */
			PIT piti = pits.get(i.getId());
			if(piti.existsUri(interest.getUri(), interest.getFragmentNumber()) == false) // Check in PITi
			{
				List<Routable> l = forwardInterest(interest, e.getLink());
				try {
					if(!piti.addEntry(interest.getUri(), interest.getFragmentNumber(), i))
						return null;
					Event event = new Event(CCNNetwork.getTime() + interest.getInterestLifeTime(), Event.Type.PIT_ENTRY_TIMEOUT, this, interest.getUri());
					event.setPackt(interest);
					event.setPitIndex(i.getId());
					network.addEvent(event);
					piti.addTimeoutEvent(interest.getUri(), interest.getFragmentNumber(), event);
					return l;
				} catch (FullPITException e1) {
					e1.printStackTrace();
					return null;
				}
			}
			if(pit.existsUri(interest.getUri(), interest.getFragmentNumber()) == false) // Shared BF
			{
				List<Routable> l = forwardInterest(interest, e.getLink());
				try {
					if(!pit.addEntry(interest.getUri(), interest.getFragmentNumber(), i))
						return null;
					/* Timeout is not scheduled since we are inserting in the shared bloom filter.
					 * Shared Bloom Filter is freed only with the RST mechanism 
					 * (see Event.Type.REFRESH_PIT handling code).
					 */
					return l;
				} catch (FullPITException e1) {
					e1.printStackTrace();
					System.err.println("[ERR] DiPIT full!");
					return null;
				}
			}
			
			return null;
		}
		else if(e.getPackt() instanceof CCNContentObject)
		{
			CCNContentObject object = (CCNContentObject) e.getPackt();
			if(object.isCachable())
			{
				if(cache.searchObject(object.getUri(), object.getFragmentNumber()) != 0)
					return null;
			}
			//cache.addElement(object.getUri(), object.getFragmentNumber(), object.getSizeInByte());
			List<Routable> r = new ArrayList<Routable>();
			it = interfaceList.iterator();
			while(it.hasNext())
			{
				Interface iFace = it.next();
				PIT piti = pits.get(iFace.getId());
				if(piti.existsUri(object.getUri(), object.getFragmentNumber()))
				{
					Event e1 = piti.getTimeoutEvent(object.getUri(), object.getFragmentNumber());
					if(e1 != null)
						e1.setCancelled(true);
					else{}  //False positive
						//System.out.println("Strange!");
					r.add(new RoutingResult(object, iFace.getLink(), this));
					Assert.assertNotNull(piti.getiFace());
					piti.removeEntry(object.getUri(), object.getFragmentNumber());
					piti.removeTimeoutEvent(object.getUri(), object.getFragmentNumber());
				}
			}
			return r;
		}
		else
			return null;
	}
	
	private List<Routable> forwardInterest(CCNInterest interest, Link link)
	{
		List<Interface> iList = fib.findRoute(interest.getUri());
		if(iList != null)
		{
			List<Routable> l = new ArrayList<Routable>();
			Iterator<Interface> iter = iList.iterator();
			while(iter.hasNext())
			{
				Interface iFace = iter.next();
				l.add(new RoutingResult(interest, iFace.getLink(), this));
				return l;
			}
		}
		else
			System.err.println("Invalid route!");
		return null;
	}
	
	public int getPitElements()
	{
		Iterator<PIT> i = pits.values().iterator();
		int res = 0;
		while(i.hasNext())
			res += i.next().getElementsInPIT();
		return res;
	}
	
	public void refreshSBF() { pit.clearAll(); Assert.assertNull(pit.getiFace()); }
	
	@Override
	protected void printInfo() {
		super.printInfo();
		Iterator<PIT> i = pits.values().iterator();
		PIT p;
		DiPIT p2;
		double res = 0;
		double queries = 0;
		int timeoutEvents = 0;
		boolean flag = false;
		while(i.hasNext())
		{
			p = (PIT)i.next();
			if(p instanceof DiPIT)
			{
				p2 = (DiPIT) p;
				res += p2.getFalsePositives();
				queries += p2.getTotalQueries();
				timeoutEvents += p.getTimeoutEvents();
				flag = true;
				if(p2.getTotalQueries() != 0 && p2.getiFace() != null)
				{
					double d = (double)p2.getFalsePositives() / (double)p2.getTotalQueries();
					d *= 100;
					System.out.println("False positives ratio for the PITi on the interface " + p2.getiFace().getId() + " (link "+p2.getiFace().getLink().getId()+") = " + dF.format(d) + " %");
				}
			}
		}
		if(pit instanceof DiPIT && ((DiPIT)pit).getTotalQueries() != 0)
		{
			double temp = (double)((DiPIT)pit).getFalsePositives() / (double)((DiPIT)pit).getTotalQueries();
			temp *= 100;
			System.out.println("False positives ratio for the shared bloom filter = " + dF.format(temp) + " %");
		}
		else
		{
			System.out.println("Total queries for the shared bloom filter = " + ((DiPIT)pit).getTotalQueries());
		}
		if(queries != 0)
		{
			//res += ((DiPIT)pit).getFalsePositives();
			//queries += ((DiPIT)pit).getTotalQueries();
			res /= queries;
			res *= 100;
			System.out.println("Overall false positives ratio for PITi = " + dF.format(res) + " %");
		}
		if(flag)
			System.out.println("Overall timeout events for PITi = " + timeoutEvents);
	}

	@Override
	public void entryTimedOut(Uri uri, int fragmentNumber, String pitIndex)
			throws LinkBusyException {
		PIT piti = pits.get(pitIndex);
		Assert.assertNotNull(piti);
		Assert.assertNotNull(piti.getiFace());
		if(piti.existsUri(uri, fragmentNumber))
		{
			//System.out.println(id + " Removing entry from PITi " + pitIndex);
			piti.removeEntry(uri, fragmentNumber); //Just remove the entry in the PIT
			piti.removeTimeoutEvent(uri, fragmentNumber);
		}
		else {}
			//System.err.println(id + " DiPIT router : Should never happen. " + uri + "/" + fragmentNumber);
	}

	@Override
	public RouterData getRouterData() {
		rData.setUsers(-1);
		rData.setPitEntries(-1);
		rData.setPitMemory(routerG.getPitMemory()*Math.pow(10, 6));
		rData.setFreeQueueDim(qSize.getFreeMem());
		rData.setMaxQueueDim(qSize.getMaxDim());
		return rData;
	}

}
