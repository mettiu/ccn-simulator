package it.polito.ccn.simulator.gui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MainFrame extends javax.swing.JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel progress;
	private JLabel labelTime;
	private JProgressBar progressBar;
	private JTextField textUsers;
	private JButton stop;
	private DecimalFormat dF;
	private boolean stopped;
	private double ramInUse;
	private double maxRam;
	//private double cpuUsage;
	//private OperatingSystemMXBean bean;

	/**
	* Auto-generated main method to display this JFrame
	*/
	/*public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainFrame inst = new MainFrame();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}*/
	
	public MainFrame() {
		super();
		initGUI();
		dF = new DecimalFormat("#.###");
		DecimalFormatSymbols d = DecimalFormatSymbols.getInstance();
		d.setDecimalSeparator('.');
		dF.setDecimalFormatSymbols(d);
		progressBar.setStringPainted(true);
		//bean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
	}
	
	public void printText(String msg)
	{
		this.progress.setText(msg);
	}
	
	public void updateTime(double time)
	{
		this.labelTime.setText("Time simulated = " + String.valueOf(dF.format(time)));
	}
	
	public void updateUsers(int users)
	{
		this.textUsers.setText("Utenti = " + String.valueOf(users));
	}
	
	public void updateProgress(int percentage)
	{
		//this.progress.setText(percentage + " %");
		ramInUse = Runtime.getRuntime().totalMemory() / Math.pow(10, 6);
		maxRam = Runtime.getRuntime().maxMemory() / Math.pow(10, 6);
		//cpuUsage = bean.getProcessCpuLoad() * 100;
		//this.progress.setText("RAM utilization = " + (int)ramInUse  + "/" + (int)maxRam + " MB  CPU usage = " + (int)cpuUsage + " %");
		this.progress.setText("RAM utilization = " + (int)ramInUse  + "/" + (int)maxRam + " MB");
		this.progressBar.setValue(percentage);
		//this.progressBar.setString(percentage + " %");
	}
	
	private void initGUI() {
		try {
			BorderLayout thisLayout = new BorderLayout();
			getContentPane().setLayout(thisLayout);
			this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			this.setTitle("Simulator");
			this.setBounds(0, 0, 318, 135);
			this.setResizable(false);
			this.setAlwaysOnTop(true);
			{
				stop = new JButton();
				getContentPane().add(stop, BorderLayout.EAST);
				stop.setText("Stop!");
				stop.addActionListener(new ActionListener () {
					 
		            public void actionPerformed(ActionEvent  e)
		            {
		                MainFrame.this.setStopped(true);
		            }
		        });
			}
			{
				progressBar = new JProgressBar();
				getContentPane().add(progressBar, BorderLayout.SOUTH);
				progressBar.setPreferredSize(new java.awt.Dimension(312, 16));
			}
			{
				textUsers = new JTextField();
				getContentPane().add(textUsers, BorderLayout.CENTER);
				textUsers.setEditable(false);
				textUsers.setText("Utenti = ");
			}
			{
				labelTime = new JLabel();
				getContentPane().add(labelTime, BorderLayout.WEST);
				labelTime.setText("Time simulated");
				labelTime.setHorizontalAlignment(JLabel.CENTER);
				labelTime.setPreferredSize(new java.awt.Dimension(175, 44));
			}
			{
				progress = new JLabel();
				getContentPane().add(progress, BorderLayout.NORTH);
				progress.setText("0 %");
				progress.setPreferredSize(new java.awt.Dimension(176, 38));
			}
			pack();
			this.setSize(412, 120);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isStopped() {
		return stopped;
	}

	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}

}
