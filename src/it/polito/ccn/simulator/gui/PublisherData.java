/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polito.ccn.simulator.gui;

/**
 *
 * @author Mettiu
 */
public class PublisherData {
    
    private String id;
    private int outElements;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOutElements() {
		return outElements;
	}

	public void setOutElements(int outElements) {
		this.outElements = outElements;
	}
    
}
