/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polito.ccn.simulator.gui;

/**
 *
 * @author Mettiu
 */
public class LinkData {
    
    private String linkId;
    private String node1;
    private boolean node1LinkStatus;
    private String node2;
    private boolean node2LinkStatus;

    public String getLinkId() {
            return linkId;
    }

    public void setLinkId(String linkId) {
            this.linkId = linkId;
    }

	public String getNode1() {
		return node1;
	}

	public void setNode1(String node1) {
		this.node1 = node1;
	}

	public boolean isNode1LinkStatus() {
		return node1LinkStatus;
	}

	public void setNode1LinkStatus(boolean node1LinkStatus) {
		this.node1LinkStatus = node1LinkStatus;
	}

	public String getNode2() {
		return node2;
	}

	public void setNode2(String node2) {
		this.node2 = node2;
	}

	public boolean isNode2LinkStatus() {
		return node2LinkStatus;
	}

	public void setNode2LinkStatus(boolean node2LinkStatus) {
		this.node2LinkStatus = node2LinkStatus;
	}
    
}
