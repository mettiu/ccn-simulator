/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polito.ccn.simulator.gui;

/**
 *
 * @author Mettiu
 */
public class RouterData {
	
	private String id;
	private int users;
	private double pitMemory;
	private int pitEntries;
	private int freeQueueDim;
	private int maxQueueDim;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getUsers() {
		return users;
	}

	public void setUsers(int users) {
		this.users = users;
	}

	public double getPitMemory() {
		return pitMemory;
	}

	public void setPitMemory(double pitMemory) {
		this.pitMemory = pitMemory;
	}

	public int getPitEntries() {
		return pitEntries;
	}

	public void setPitEntries(int pitEntries) {
		this.pitEntries = pitEntries;
	}

	public int getMaxQueueDim() {
		return maxQueueDim;
	}

	public void setMaxQueueDim(int maxQueueDim) {
		this.maxQueueDim = maxQueueDim;
	}

	public int getFreeQueueDim() {
		return freeQueueDim;
	}

	public void setFreeQueueDim(int freeQueueDim) {
		this.freeQueueDim = freeQueueDim;
	}
   
}
