/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polito.ccn.simulator.gui;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.DefaultCaret;

/**
 *
 * @author Mettiu
 */
public class MainGui extends javax.swing.JFrame {
    
    public class MyTableCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            setBackground(null);
            //super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            Component cell = super.getTableCellRendererComponent (table, value, isSelected, hasFocus, row, column);
            if(column == 2 || column == 4)
            {
                if(String.valueOf(value).equals("true"))
                {
                    cell.setBackground( Color.RED );
                    setText("Busy");
                }
                else
                {
                    cell.setBackground( Color.GREEN );
                    setText("Free");
                }
            }
            return this;
        }

    }
   
   /**
	 * 
	 */
   private static final long serialVersionUID = 1L;
   private boolean stopped;
   private boolean log = false;
   private DecimalFormat dF;
   private String[] columnNames = {"Node ID",
                        "Active Users",
                        "PIT Entries",
                        "PIT Mem Occupancy",
                        "Queue size"};
   private String[] linkColumnNames = {"Link ID",
                        "Node 1 ID",
                        "Status (Uplink node1)",
                        "Node2 ID",
                        "Status (Uplink node2)"};
   private String[] publisherColumnNames = {"Provider ID",
                        "Elements in queue (OUT)"};
   private DefaultTableModel routerModel;
   private DefaultTableModel linkModel;
   private DefaultTableModel publisherModel;
   //private double cpuUsage;
   //private OperatingSystemMXBean bean;

   /**
    * Creates new form MainGui
    */
   public MainGui() {
      initComponents();
      DefaultCaret caret = (DefaultCaret)jTextArea1.getCaret();
      caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
      this.scrollCheckBox.setSelected(true);
      this.enableCheckBox.setSelected(false);
      this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      this.setLocationRelativeTo(null);
      this.setTitle("Simulator");
      this.setResizable(false);
      this.setAlwaysOnTop(true);
      this.jCheckBox1.setEnabled(false);
      stopped = false;
      dF = new DecimalFormat("#.##");
      DecimalFormatSymbols d = DecimalFormatSymbols.getInstance();
      d.setDecimalSeparator('.');
      dF.setDecimalFormatSymbols(d);
      jProgressBar1.setStringPainted(true);
      Object[][] array = new Object[1][4];
      Object[][] array2 = new Object[1][3];
      routerModel = new DefaultTableModel(array, columnNames) {
		private static final long serialVersionUID = 1L;

		@Override
                public boolean isCellEditable(int row, int column) {
                   //all cells false
                   return false;
                }
      };
      linkModel = new DefaultTableModel(array2, linkColumnNames) {
		private static final long serialVersionUID = 1L;

		@Override
                public boolean isCellEditable(int row, int column) {
                   //all cells false
                   return false;
                }
      };
      publisherModel = new DefaultTableModel(array2, publisherColumnNames) {
		private static final long serialVersionUID = 1L;

		@Override
                public boolean isCellEditable(int row, int column) {
                   //all cells false
                   return false;
                }
      };
      jTable1.setModel(routerModel);
      jTable2.setModel(linkModel);
      jTable3.setModel(publisherModel);
      routerModel.setRowCount(0);
      linkModel.setRowCount(0);
      publisherModel.setRowCount(0);
      //bean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
   }
   
   public void printText(String msg) { jLabel1.setText(msg); }
   
   public void setNetworkStable(boolean state) { this.jCheckBox1.setSelected(state); }
   
   public void updateTime(double time) { this.jLabel2.setText("Time (sim) = " + String.valueOf(dF.format(time))); }
   
   public void updateUsers(int users) { this.jLabel3.setText("Utenti = " + String.valueOf(users));}
   
   public void updateProgress(int percentage) {
      double ramInUse = Runtime.getRuntime().totalMemory() / Math.pow(10, 6);
      double maxRam = Runtime.getRuntime().maxMemory() / Math.pow(10, 6);
      this.jLabel1.setText("RAM utilization = " + (int)ramInUse  + "/" + (int)maxRam + " MB");
      this.jProgressBar1.setValue(percentage);
      //cpuUsage = bean.getProcessCpuLoad() * 100;
      //this.jLabel1.setText("RAM utilization = " + (int)ramInUse  + "/" + (int)maxRam + " MB  CPU usage = " + (int)cpuUsage + " %");
   }
   
   public void logOnConsole(String text)
   {
       if(log)
           jTextArea1.append(text + "\n");
   }
   
   public void updateRouterTable(List<RouterData> data)
   {
      Iterator<RouterData> it = data.iterator();
      RouterData r;
      routerModel.setRowCount(0);
      while(it.hasNext())
      {
         r = it.next();
         if(r.getFreeQueueDim() == 0)
             routerModel.addRow(new Object[]{r.getId(), r.getUsers(), r.getPitEntries(), dF.format(r.getPitMemory()), "Inf"});
         else
             routerModel.addRow(new Object[]{r.getId(), r.getUsers(), r.getPitEntries(), dF.format(r.getPitMemory()), (r.getMaxQueueDim() - r.getFreeQueueDim())});
      }
   }
   
   public void updateLinkTable(List<LinkData> data)
   {
        Iterator<LinkData> it = data.iterator();
        LinkData l;
        linkModel.setRowCount(0);
        while(it.hasNext())
        {
           l = it.next();
               linkModel.addRow(new Object[]{l.getLinkId(), l.getNode1(), l.isNode1LinkStatus(), l.getNode2(), l.isNode2LinkStatus()});
        }
        Enumeration<TableColumn> en = jTable2.getColumnModel().getColumns();
        while (en.hasMoreElements()) {
            TableColumn tc = en.nextElement();
            tc.setCellRenderer(new MyTableCellRenderer());
        }
   }
   
    public void updateProviderTable(List<PublisherData> data)
    {
        Iterator<PublisherData> it = data.iterator();
        PublisherData l;
        publisherModel.setRowCount(0);
        while(it.hasNext())
        {
           l = it.next();
               publisherModel.addRow(new Object[]{l.getId(), l.getOutElements()});
        }
    }
   
    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }
   

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jProgressBar1 = new javax.swing.JProgressBar();
        jCheckBox1 = new javax.swing.JCheckBox();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        scrollCheckBox = new javax.swing.JCheckBox();
        clearButton = new javax.swing.JButton();
        enableCheckBox = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("RAM Utilization = ");

        jLabel2.setText("Time (sim) = ");

        jLabel3.setText("Utenti = ");

        jButton1.setText("Stop");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jCheckBox1.setText("Net Stable");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Node ID", "Active Users", "PIT entries", "PIT Mem Occupancy", "Queue Size"
            }
        ) {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			@SuppressWarnings("rawtypes")
			Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.String.class
            };

            @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jTabbedPane2.addTab("Routers", jScrollPane1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Link_ID", "Node1 ID", "Status (Uplink node1)", "Node2 ID", "Status (Uplink node2)"
            }
        ) {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			@SuppressWarnings("rawtypes")
			Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class
            };

            @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jTabbedPane2.addTab("Links", jScrollPane2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Provider ID", "Elements in queue (OUT)"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jTabbedPane2.addTab("Content Providers", jScrollPane3);

        jTextArea1.setEditable(false);
        jTextArea1.setBackground(new java.awt.Color(240, 240, 240));
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane5.setViewportView(jTextArea1);

        scrollCheckBox.setText("Auto scroll");
        scrollCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scrollCheckBoxActionPerformed(evt);
            }
        });

        clearButton.setText("Clear");
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });

        enableCheckBox.setText("Enable");
        enableCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enableCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(enableCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollCheckBox)
                .addGap(18, 18, 18)
                .addComponent(clearButton)
                .addContainerGap(568, Short.MAX_VALUE))
            .addComponent(jScrollPane5)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(scrollCheckBox)
                    .addComponent(clearButton)
                    .addComponent(enableCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Console", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jCheckBox1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addComponent(jTabbedPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        MainGui.this.setStopped(true);
   }//GEN-LAST:event_jButton1ActionPerformed

    private void scrollCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scrollCheckBoxActionPerformed
        // TODO add your handling code here:
        if(this.scrollCheckBox.isSelected())
        {
            DefaultCaret caret = (DefaultCaret)jTextArea1.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            jTextArea1.setCaretPosition(jTextArea1.getDocument().getLength());
        }
        else
        {
            DefaultCaret caret = (DefaultCaret)jTextArea1.getCaret();
            caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        }
    }//GEN-LAST:event_scrollCheckBoxActionPerformed

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
        // TODO add your handling code here:
        jTextArea1.setText("");
    }//GEN-LAST:event_clearButtonActionPerformed

    private void enableCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enableCheckBoxActionPerformed
        // TODO add your handling code here:
        if(this.enableCheckBox.isSelected())
            log = true;
        else
            log = false;
    }//GEN-LAST:event_enableCheckBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clearButton;
    private javax.swing.JCheckBox enableCheckBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JCheckBox scrollCheckBox;
    // End of variables declaration//GEN-END:variables
}
