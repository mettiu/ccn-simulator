package it.polito.ccn.simulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FIB {
	
	private Map<String, Uri> stringToUri;
	private Map<Uri, List<Interface>> fib;
	
	public FIB()
	{
		stringToUri = new LinkedHashMap<String, Uri>();
		/* LinkedHashMap will iterate in the order in which the entries were put into the map. 
		 */
		fib = new LinkedHashMap<Uri, List<Interface>>();
	}
	
	public int getFibElement() { return stringToUri.size(); }
	
	public void printFIB()
	{
		Iterator<String> i = stringToUri.keySet().iterator();
		String s;
		Uri u;
		System.out.println("****START FIB******");
		while(i.hasNext())
		{
			s = i.next();
			u = stringToUri.get(s);
			System.out.println(u);
			Iterator<Interface> it = fib.get(u).iterator();
			while(it.hasNext())
			{
				Interface iFace = it.next();
				System.out.println("\tInterface "+iFace.getId());
			}
		}
		System.out.println("****EDN******");
	}
	
	public boolean addRoute(Uri uri, Interface iface)
	{
		List<Interface> list;
		Uri u = stringToUri.get(uri.toString());
		if(u == null)
		{
			list = new ArrayList<Interface>();
			list.add(iface);
			fib.put(uri, list);
			stringToUri.put(uri.toString(), uri);
			return true;
		}
		else
		{
			list = fib.get(u);
			if(list != null)
			{
				list.add(iface);
				return true;
			}
		}
		return false;
	}
	
	public List<Interface> findRoute(Uri uri)
	{
		String stringUri;
		Uri u;
		Iterator<String> i = stringToUri.keySet().iterator();
		while(i.hasNext())
		{
			stringUri = i.next();
			u = stringToUri.get(stringUri);
			if(uri.match(u))
				return fib.get(u);
		}
		return null;
	}

}
