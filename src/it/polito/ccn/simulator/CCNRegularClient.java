package it.polito.ccn.simulator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.junit.Assert;

public class CCNRegularClient extends CCNNode{
	
	public enum State {
		DOWNLOADING, SLEEPING
	}
	
	private final int LIFETIME_60S_RATE = 0;
	private final int pipelineDim;
	
	private final Interface iFace;
	
	private int realChunks;
	private int objectsReceived;
	private int currentPipelineDim;
	private int interestsSent;
	private int interestsToSent;
	private int lastDataPacketDim;
	private int numChunks;
	
	private State state;
	
	private Destination d;
	
	private boolean finishing;
	
	private Uri destination;
	
	private Link link;
	
	private CCNEdgeRouter router;
	
	private double startTime;
	
	private UniformIntegerDistribution dist;
	
	/* Variables for client stall checking */
	private int tempInterestsSent;
	private byte stalling;
	
	public CCNRegularClient(CCNNetwork network, String id, List<Link> links, double rate, CCNEdgeRouter router, Interface iFace)
	{
		/* Client rate (in Mbps) */
		super(network, id, links);
		Assert.assertNotNull(iFace);
		Assert.assertTrue(links.size() == 1);	// Only one link allowed
		Assert.assertTrue(links.get(0) != null);
		this.link = links.get(0);
		this.router = router;
		this.iFace = iFace;
		//TODO: Rechech this check
		if(rate > link.getCapacity(router.getId()))
		{
			rate = link.getCapacity(router.getId());
			System.err.println("Rate = " + rate + " - Band = " + link.getCapacity(id));
			System.err.println("[WARNING] Network misconfiguration. User connected to link "+link.getId()+" generates traffic at a non-feasible rate.");
		}
		link.addNode(this);
		state = State.SLEEPING;
		ingressQueues.put(link.getId(), new Queue());
		egressQueues.put(link.getId(), new Queue());
		pipelineDim = network.getClientPipelineSize();
		currentPipelineDim = 0;
		finishing = false;
		router.incrementRequests();
		dist = new UniformIntegerDistribution(1, 100);
	}
	
	public Routable deletePacketFromPipeline()
	{
		currentPipelineDim--;
		Assert.assertTrue(currentPipelineDim >= 0);
		/* Emulating retransmitted packets */
		interestsToSent++;
		numChunks++;
		objectsReceived++;
		finishing = false;
		CCNInterest interest = nextInterest();
		if(interest != null)
		{
			interest.setCachable(false);
			return new RoutingResult(interest, link, this);
		}
		else
		{
			System.err.println("Client stalling " + id);
			return null;
		}
	}
	
	public State getState() { return state; }
	
	public int getPipelineSize() { return currentPipelineDim; }
	
	public CCNInterest nextInterest()
	{
		CCNInterest interest = null;
		int sample;
		switch(state)
		{
			case SLEEPING:
				/* Select file (an index for the array containing all the contents) */
				int fileIndex = network.getZipfDistribution().nextInt();
				d = network.getDestinationFiles().get(fileIndex);
				/* I get the final destination */
				CCNPublisher publisher = network.getGoodPublishers().get(fileIndex % network.getGoodPublishers().size());
				/* I get the prefix */
				destination = publisher.getDestinations().get(0);
				/* Compute the total number of chunks to be requestes */
				numChunks = d.getNumChunks();
				/* Generate the first Interest */
				interest = new CCNInterest(d.getUri(), 0, network.getDefaultInterestLifeTime(), network.getPacketHeaderSizeInByte(), network.getMaxPayloadPerContent(), this);
				/* Update the state of the client */
				interestsToSent = numChunks;
				tempInterestsSent = 0;
				stalling = 0;
				realChunks = numChunks;
				interestsSent = 0;
				objectsReceived = 0;
				state = State.DOWNLOADING;
				currentPipelineDim = 1;
				network.incrementUsersActive();
				startTime = CCNNetwork.getTime();
				if(interestsToSent > 1)
					finishing = false;
				else
					finishing = true;
				break;
			case DOWNLOADING:
				/* If I'm going to finish, I'm not generating Interest anymore */
				if(finishing)
					return null;
				/* Pipeline is full -> DO NOT send Interest anymore */
				if(currentPipelineDim >= pipelineDim)
					return null;
				interestsSent++;
				/* Set the fragment number that I want to retrieve */
				if(interestsSent >= interestsToSent-1)
				{
					/* Send the last Interest packet */
					interest = new CCNInterest(d.getUri(), interestsSent, network.getDefaultInterestLifeTime(), network.getPacketHeaderSizeInByte(), lastDataPacketDim, this);
					interest.setIsLastChunk(true);
					finishing = true;
				}
				else
				{
					interest = new CCNInterest(d.getUri(), interestsSent, network.getDefaultInterestLifeTime(), network.getPacketHeaderSizeInByte(), network.getMaxPayloadPerContent(), this);
					finishing = false;
				}
				/* Update the state of the client */
				currentPipelineDim++;
				break;
			default:
				break;
		}
		sample = dist.sample();
		if(sample < LIFETIME_60S_RATE)
			interest.setInterestLifeTime(60);
		return interest;
	}

	@Override
	public List<Routable> processPacket(Event e) {
		CCNInterest interest;
		CCNContentObject object;
		/* Unsolicited data packet */
		if(currentPipelineDim == 0)
		{
			System.err.println("Time="+CCNNetwork.getTime()+" Empty pipeline. It was " + ((CCNContentObject)e.getPackt()).getUri() + " but I'm downloading "+d.getName()+interestsSent);
			network.getLogger().log("Time="+CCNNetwork.getTime()+" Empty pipeline. It was " + ((CCNContentObject)e.getPackt()).getUri() + " but I'm downloading "+d.getName()+interestsSent+". File grande=" + d.getNumBytes() + ". Last chunk="+((CCNContentObject)e.getPackt()).isLastChunk(), CCNNetwork.getTime(), Logger.WARNING);
			if(e.getPackt() instanceof CCNContentObject && !((CCNContentObject)e.getPackt()).isLastChunk())
				return null;
		}
		if(e.getPackt() instanceof CCNContentObject)
		{
			object = (CCNContentObject) e.getPackt();
			if(!object.getUri().match(destination))
			{
				System.err.println(object.getUri() + " doesn't match " + destination + "! What I received? A false positive?");
				return null;
			}
			objectsReceived++;
			/*if(realChunks < numChunks)
				System.err.println(id + ". Arrivato "+ object.getUri() + " di " + numChunks + " totali. Ricevuti = " + objectsReceived);*/
			if(objectsReceived >= numChunks)
			{
				Assert.assertTrue(numChunks > 0);
				ClientStats stats = new ClientStats(numChunks, realChunks, startTime, CCNNetwork.getTime(), d.getNumBytes());
				reset();
				this.link = null;
				network.decrementUsersActive();
				router.incrementDepartures();
				router.destroyUser(this, iFace, stats);
				network.logOnConsole(id + ". Time = " + (CCNNetwork.getTime() - startTime) + " I downloaded file " + d.getName() + " - Useful Bytes = " + d.getNumBytes());
				return null;
			}
			else
			{
				currentPipelineDim--;
				Assert.assertTrue(currentPipelineDim >= 0);
				interest = nextInterest();
				if(interest != null)
				{
					List<Routable> l = new ArrayList<Routable>();
					l.add(new RoutingResult(interest, link, this));
					return l;
				}
				else
					return null;
			}
		}
		else
			return null;
	}
	
	public ClientStats getStats()
	{
		ClientStats stats = new ClientStats(numChunks, realChunks, startTime, -1, d.getNumBytes());
		return stats;
	}
	
	public boolean isStalling()
	{
		if(tempInterestsSent == interestsSent)
			stalling++;
		tempInterestsSent = interestsSent;
		if(stalling > 3)
			return true;
		return false;
	}

	public void reset() 
	{
		interestsSent = 0;
		objectsReceived = 0;
		lastDataPacketDim = 0;
		state = State.SLEEPING;
		currentPipelineDim = 0;
		finishing = false;
		realChunks = 0;
	}
	
	@Override
	public double getServiceTime() {
		return 0.0;
	}

	@Override
	public void terminate() {
		return;
	}

}
