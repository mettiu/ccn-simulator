package it.polito.ccn.testing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.junit.Test;

public class TestDistribution {
	
	private ExponentialDistribution random;
	private DecimalFormat dF;
	
	public TestDistribution()
	{
		random = new ExponentialDistribution(1);
		dF = new DecimalFormat("#.###########");
	}
	
	/*
	 * N.B. Nel file di uscita exponential.dat vanno sostituite le virgole con i punti
	 * altrimenti GnuPlot impazzisce e non printa correttamente i valori!
	 */
	@Test
	public void testExponentialDistribution() throws IOException
	{
		File f = new File("exponential.dat");
		if(f.exists())
			f.delete();
		f.createNewFile();
		PrintWriter pWriter = new PrintWriter(new FileWriter(f), true);
		int samples = 10000000;
		int values_dim = 8000;
		Double values[] = new Double[values_dim];
		int i;
		double d;
		int temp;
		for(i=0; i<samples; i++)
		{
			d = random.sample();
			d *= 1000;
			if(d>values_dim-1)
				continue;
			temp = (int) d;
			if(values[temp] == null)
				values[temp] = new Double(1);
			else
				values[temp] = new Double(values[temp]+1);
		}
		for(i=0; i<values.length; i++)
		{
			if(values[i] != null)
			{
				values[i] /= samples;
				
				pWriter.append(i + "\t" + dF.format(values[i]) + "\r\n");
			}
			else
				pWriter.append(i + "\t" + new Integer(0) + "\r\n");
		}
		pWriter.flush();
		pWriter.close();
	}

}
