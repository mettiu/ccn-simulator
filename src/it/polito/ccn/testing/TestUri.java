package it.polito.ccn.testing;

import static org.junit.Assert.*;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import org.junit.Test;
import org.xml.sax.SAXException;

public class TestUri {
	
	@SuppressWarnings("unused")
	private CCNNetwork network;
	
	public TestUri() throws JAXBException, SAXException
	{
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "false");
	}

	@Test
	public void test1() throws BadUriException {
		String[] uri = {"polito.it", "video", "prova", "example.avi", "s0"};
		String[] wildcard = {"polito.it", "video", "prova", "example.avi", "*"};
		String[] u = {"polito.it", "video", "prova", "example.avi", "s43"};
		Uri uri1 = new Uri(uri);
		Uri uri2 = new Uri(wildcard);
		Uri uri3 = new Uri(u);
		assertEquals(true, uri1.match(uri2));
		assertEquals(true, uri2.match(uri1));
		assertEquals(false, uri3.match(uri1));
		assertEquals(false, uri1.match(uri3));
	}
	
	@Test
	public void test2() throws BadUriException {
		String[] hacker_uri = {"hacker.it", "attack", "a4585745913718191398052296751641597404933220245921676602463046972868495784089815410397449476163104012566702462513187996625265293832289307359080454111033046857808014099531346285970523918235759940447271916414064529884195778486801439910905918722453258269747090146539304764482865106467368113939140492096657424011328691234059746159409374206050703447764626483571817323225626504503443862390948731862057889548862576751152674926533236923819254193245221089510958107393131275288533093706554066128021582609675079984853320925276390310594862461317625015233004929712271920566795656384017111187078170458352268693061516069009297942200159946419553717754408160720538884951831133297970251437699804524010483903291684389989742040819902059562153476183346116224526655639785893281312715027214558845346589005423656035827416661545563204190400219371745822344778700677279064397768890535157046984300996617027230936697647984668044882031432613996596857360653786353534991216438918232119644392060951097629979226329780897292711158065042", "s0"};
		String[] uri = {"polito.it", "video", "example.avi", "s41"};
		Uri uri1 = new Uri(hacker_uri);
		Uri uri2 = new Uri(uri);
		assertEquals(false, uri1.match(uri2));
		assertEquals(false, uri2.match(uri1));
	}

}
