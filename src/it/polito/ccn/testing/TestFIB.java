package it.polito.ccn.testing;

import static org.junit.Assert.*;

import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.CCNCoreRouter;
import it.polito.ccn.simulator.FIB;
import it.polito.ccn.simulator.Interface;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import org.junit.Test;
import org.xml.sax.SAXException;

public class TestFIB {
	
	private FIB fib;
	private CCNNetwork network;
	
	public TestFIB() throws JAXBException, SAXException
	{
		fib = new FIB();
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "true");
	}

	@Test
	public void testFIB1() throws BadUriException {
		CCNCoreRouter router = (CCNCoreRouter)network.getRouters().get(0);
		String[] components = {"it", "polito", "video", "*"};
		Uri uri = new Uri(components);
		uri.setId(0);
		fib.addRoute(uri, router.getInterfaceFromId("31"));
		String[] components2 = {"it", "polito", "audio", "*"};
		Uri uri2 = new Uri(components2);
		uri2.setId(1);
		fib.addRoute(uri2, router.getInterfaceFromId("31"));
		assertEquals(2, fib.getFibElement());
		String[] uriToRoute = {"it", "polito", "video", "movie.avi"};
		Uri uri3 = new Uri(uriToRoute);
		uri3.setId(2);
		List<Interface> list = fib.findRoute(uri3);
		assertEquals(1, list.size());
		Interface iFace = list.get(0);
		assertEquals("31", iFace.getId());
		String[] uriToRoute2 = {"it", "polito", "audio", "song.mp3"};
		Uri uri4 = new Uri(uriToRoute2);
		uri4.setId(3);
		list = fib.findRoute(uri4);
		assertEquals(1, list.size());
		iFace = list.get(0);
		assertEquals("31", iFace.getId());
	}
	
	@Test
	public void testFIB2() throws Exception {
		CCNCoreRouter router = (CCNCoreRouter)network.getRouters().get(0);
		String[] components = {"polito.it", "video", "*"};
		Uri uri = new Uri(components);
		uri.setId(0);
		fib.addRoute(uri, router.getInterfaceFromId("21"));
		fib.addRoute(uri, router.getInterfaceFromId("31"));
		fib.addRoute(uri, router.getInterfaceFromId("11"));
		String[] components2 = {"polito.it", "audio", "*"};
		Uri uri2 = new Uri(components2);
		uri2.setId(1);
		fib.addRoute(uri2, router.getInterfaceFromId("21"));
		fib.addRoute(uri2, router.getInterfaceFromId("11"));
		assertEquals(2, fib.getFibElement());
		String[] uriToRoute = {"polito.it", "video", "movie.avi"};
		Uri uri3 = new Uri(uriToRoute);
		uri3.setId(2);
		List<Interface> list = fib.findRoute(uri3);
		assertEquals(3, list.size());
		String[] uriToRoute2 = {"polito.it", "audio", "song.mp3"};
		Uri uri4 = new Uri(uriToRoute2);
		uri4.setId(3);
		list = fib.findRoute(uri4);
		assertEquals(2, list.size());
	}

}
