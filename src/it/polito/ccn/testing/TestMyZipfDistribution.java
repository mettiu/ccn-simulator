package it.polito.ccn.testing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Test;

import it.polito.ccn.simulator.ZipfDistribution;

public class TestMyZipfDistribution {
	
	private ZipfDistribution zipf;
	private int totalElements;
	private double q;
	private double alpha;
	
	public TestMyZipfDistribution()
	{
		totalElements = 100;
		alpha = 0.55;
		q = 25;
		zipf = new ZipfDistribution(1234551816, totalElements, alpha, q);
	}
	
	@Test
	public void printDensityCumulative()
	{		
		try {
			/* Apertura file di log in append mode */
			File f = new File("zipf.dat");
			if(f.exists())
				f.delete();
			f.createNewFile();
			PrintWriter pWriter = new PrintWriter(new FileWriter(f), true);
			int i;
			for(i=0; i<totalElements; i++)
				pWriter.append(i + "\t" + zipf.getDensity()[i] + "\t\t" + zipf.getCumulativeFunction()[i] + "\r\n");
			pWriter.flush();
			pWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Impossibile aprire il file di log in scrittura");
		}
	}
	
	@Test
	public void printDensity() throws InterruptedException
	{
		try {
			/* Apertura file di log in append mode */
			File f = new File("zipfdensity.dat");
			if(f.exists())
				f.delete();
			f.createNewFile();
			PrintWriter pWriter = new PrintWriter(new FileWriter(f), true);
			int i, temp;
			final int samples = 1000000;
			Double values[] = new Double[totalElements];
			for(i=0; i<samples; i++)
			{
				temp = zipf.nextInt();
				if(values[temp] == null)
					values[temp] = new Double(1);
				else
					values[temp] = new Double(values[temp]+1);
			}
			for(i=0; i<totalElements; i++)
			{
				if(values[i] != null)
				{
					values[i] /= samples;
					pWriter.append(i + "\t" + values[i] + "\r\n");
				}
				else
					pWriter.append(i + "\t" + new Integer(0) + "\r\n");
			}
			pWriter.flush();
			pWriter.close();
			Process p = Runtime.getRuntime().exec("gnuplot script_gnuplot.gp");
			p.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Impossibile aprire il file di log in scrittura");
		}
	}

}
