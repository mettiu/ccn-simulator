package it.polito.ccn.testing;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import it.polito.ccn.simulator.BloomFilter;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.exception.CounterOverflowException;

public class TestBloomFilter {
	
	private final int filterSize = 10000;
	
	public TestBloomFilter()
	{
		
	}
	
	@Test
	public void test1() throws CounterOverflowException
	{
		/*filter.add("DATA1");
		filter.add("DATA2");
		//filter.printFilter();
		filter.remove("DATA1");
		filter.remove("DATA2");
		//filter.printFilter(); */
	}
	
	@Test
	public void test2()
	{
		/* false positive probability = (1-e^(-k*n/m))^k */
		// n = 10000
		// m = 5.000.000
		// k = 4 (HARD-CODED)
		// p = 4.031*10^(-9)
		Assert.assertTrue(BloomFilter.getFalsePositiveProbability(10000, 5000000) > 4.03*Math.pow(10, -9));
		Assert.assertTrue(BloomFilter.getFalsePositiveProbability(10000, 5000000) < 4.032*Math.pow(10, -9));
	}
	
	/*@Test
	public void test3()
	{
		UniformRealDistribution dist = new UniformRealDistribution();
		int i;
		double false_positives = 0;
		int MAX_TRY = 100000000;
		double p = BloomFilter.getFalsePositiveProbability(10000, 5000000);
		for(i=0; i<MAX_TRY; i++)
		{
			//System.out.println(dist.sample());
			if(dist.sample() <= p)
				false_positives++;
		}
		double temp = (double)false_positives / (double)MAX_TRY; 
		System.out.println("Global false positive probability = " + temp);
	}
	
	@Test
	public void test4() throws CounterOverflowException
	{
		int i, j, length = 100;
		char[] array = new char[length];
		String s;
		for(i=0; i<200; i++)
		{
			for(j=0; j < length; j++)
				array[j] = (char) (j);
			s = new String(array);
			filter.add(s);
		}
		for(i=0; i<200; i++)
		{
			for(j=0; j < length; j++)
				array[j] = (char) (j);
			s = new String(array);
			filter.remove(s);
		}
	}
	
	@Test
	public void test5() throws CounterOverflowException
	{
		UniformIntegerDistribution dist2 = new UniformIntegerDistribution(97, 122);
		int i, j, length = 100;
		char[] array = new char[length];
		List<String> strings = new ArrayList<>();
		String s;
		for(i=0; i<2000000; i++)
		{
			for(j=0; j < length; j++)
				array[j] = (char) (dist2.sample());
			s = new String(array);
			strings.add(s);
			filter.add(s);
		}
		for(i=0; i<2000000; i++)
		{
			Assert.assertTrue(filter.contains(strings.get(i)));
			filter.remove(strings.get(i));
		}
		for(i=0; i<filterSize; i++)
			Assert.assertTrue(filter.getCounterAt(i) == 0);
	} */
	
	@Test
	public void test6() throws NoSuchAlgorithmException, BadUriException
	{
		MessageDigest dig = MessageDigest.getInstance("SHA-1");
		String[] components = {
				"it",
				"badPubRo",
				"a0",
				"s0"
		};
		Uri uri = new Uri(components);
		uri.setUriSizeInByte(20);
		uri.setId(UUID.randomUUID().hashCode());
		uri.initArray(1500, 1500);
		byte[] b = dig.digest(uri.getUriWithFragmentNumber(uri.getFragmentNumber()).getBytes());
		int i;
		int[] hashes = new int[4];
		for(i=0; i<hashes.length; i++)
		{
			hashes[i] = Math.abs((b[i*5] << 32) | (b[i+1] << 24) | (b[i+2] << 16) | (b[i+3] << 8) | b[i+4]);
			System.out.println(hashes[i] % filterSize);
		}
	}

}
