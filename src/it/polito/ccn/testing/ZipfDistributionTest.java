package it.polito.ccn.testing;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.ZipfDistribution;

public class ZipfDistributionTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ZipfDistribution zipf = new ZipfDistribution(100, 1);
		int i;
		for(i=0; i<1000; i++)
			System.out.println("Estrazione=" + zipf.sample());
		
		System.out.println("******Dimensioni dei files*******");
		
		UniformIntegerDistribution dist = new UniformIntegerDistribution((int)Math.pow(10, 3), (int)Math.pow(10, 9));
		for(i=0; i<100000; i++)
			System.out.println(dist.sample() + " bytes");
		System.out.println("Media dimensione file = " + dist.getNumericalMean());
	}

}
