package it.polito.ccn.testing;

import java.util.Date;

import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.Logger;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class RunningTest {
	
	//private static final String file = "/resources/NetworkConfiguration.xml";
	//private static final String file = "/resources/testRetransmission.xml";
	//private static final String file = "/resources/telecomTopology.xml";
	//private static final String file = "/resources/telecomTopologyMini.xml";
	//private static final String file = "/resources/simpleTopology.xml";

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		if(args.length < 1)
		{
			System.err.println("Usage: prog_name configurationFile.xml");
			return;
		}
		String file = args[0];
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		try {
			schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		} catch (SAXException e) {
			System.out.println("Invalid schema.");
		}
		u.setSchema(schema);
		System.out.println("Selected file = " + file);
		System.out.println("URL = " + u.getClass().getResource(file));
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource(file));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		String attackerUriDim = System.getProperty("attackerUriDim");
		String routerType = System.getProperty("routerType");
		String pitType = System.getProperty("pitType");
		String gui = System.getProperty("enableGui");
		Runnable network = new CCNNetwork(ccnConf, attackerUriDim, routerType, pitType, gui);
		Thread t = new Thread(network);
		CCNNetwork net = (CCNNetwork) network;
		net.getLogger().log("Seelcted file = " + file, 0, Logger.EVER);
		net.getLogger().log("URL = " + u.getClass().getResource(file), 0, Logger.EVER);
		t.start();
		System.out.println("****Network started****");
		try 
		{
			t.join();
			System.out.println("-=Simulation ended=-");
			System.out.println(new Date());
			System.exit(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
