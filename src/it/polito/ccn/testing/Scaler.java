package it.polito.ccn.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Scaler {
	
	public static void main(String  args[]) {
		FileInputStream  fstream;
		DataInputStream  in = null;
		BufferedReader  br;
		FileWriter fw;
        BufferedWriter bw = null;
        PrintWriter outFile = null;
        try
        {
        	fstream = new FileInputStream("logs/users.txt");
    		in = new DataInputStream(fstream);
    		br = new BufferedReader(new InputStreamReader(in));
    		fw = new FileWriter("logs/users1000.txt");
            bw = new BufferedWriter(fw);
            outFile = new PrintWriter(bw);
            String strLine;
            String[] res;

            while((strLine = br.readLine()) != null)
            {
                System.out.println(strLine);
                res = strLine.split("\\s+");
                res[2] = String.valueOf(Integer.parseInt(res[2])*1000);
                //res[2] = String.valueOf(Double.parseDouble(res[2])*100);
                //res[3] = String.valueOf(Double.parseDouble(res[3])*100);
                StringBuffer result = new StringBuffer();
                for (int i = 0; i < res.length; i++) {
                   result.append(res[i] + "\t");
                   //result.append( optional separator );
                }
                String mynewstring = result.toString();
                outFile.println(mynewstring);
            }
        }
        catch(Exception e){
            System.err.println("Errore: " + e);
        }
        finally
        {
        	try {
				in.close();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
            outFile.flush();
            outFile.close();
        }
    }
	
}
