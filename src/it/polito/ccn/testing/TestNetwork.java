package it.polito.ccn.testing;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import org.junit.Test;
import org.xml.sax.SAXException;

public class TestNetwork {
	
	private CCNNetwork network;
	
	public TestNetwork() throws JAXBException, SAXException {
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/testRouter.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "false");
	}

	@Test
	public void test() throws LinkBusyException, InterruptedException {
		Thread t = new Thread(network);
		t.start();
		t.join();
	}

}
