package it.polito.ccn.testing;

import static org.junit.Assert.*;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.CCNCoreRouter;
import it.polito.ccn.simulator.SimplePIT;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.exception.FullPITException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

public class TestPIT {
	
	private SimplePIT pit;
	private CCNNetwork network;
	private CCNCoreRouter router;
	
	public TestPIT() throws JAXBException, SAXException
	{
		pit = new SimplePIT(1);
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "1000", "Core", "SimplePIT", "false");
		router = (CCNCoreRouter)network.getRouters().get(1);	// Prendo R2
		/* Vedi file NetworkConfiguration.xml -> il router testato � R2 */
		Assert.assertEquals("r2", router.getId());
	}

	@Test
	public void testPIT1() throws FullPITException, BadUriException {
		String[] components = {"polito.it", "video", "prova", "example.avi", "s0"};
		Uri uri = new Uri(components);
		uri.initArray(20000, 1500);
		uri.setId(0);
		uri.initArray(10000, 1500);
		pit.addEntry(uri, 0, router.getInterfaceFromId("21"));
		assertEquals(1, pit.getElementsInPIT());
		assertEquals(true, pit.existsUri(uri, 0));
		pit.addEntry(uri, 0, router.getInterfaceFromId("11"));
		assertEquals(1, pit.getElementsInPIT());
		assertEquals(2, pit.getInterfacesPending(uri, 0).size());
		pit.removeEntry(uri, 0);
		assertEquals(0, pit.getElementsInPIT());
		assertEquals(false, pit.existsUri(uri, 0));
		System.out.println("Test passed!");
	}
	
	@Test
	public void testPIT2() throws FullPITException, BadUriException {
		String[] fake_uri = new String[1];
		Uri uri;
		fake_uri[0] = "";
		int i, j;
		for(i=0; i<999; i++)
		{
			fake_uri[0] = "";
			/* Produco una uri lunga esattamente 125 caratteri */
			for(j=0; j<125; j++)
				fake_uri[0] = fake_uri[0].concat(String.valueOf(Math.abs((int)(Math.random()*10)-1)));
			uri = new Uri(fake_uri);
			uri.initArray(1500, 1500);
			uri.setId(i);
			pit.addEntry(uri, i, router.getInterfaceFromId("21"));
		}
		/* Rimangono 875125 bytes liberi dopo aver inserito 999 URI di lunghezza fissa 
		 * PIT = 1 MB = 1 * 10^6 bytes
		 * |Entry inserite| = (999*125) bytes = 124875 bytes
		 * 10^6 - 124875 = 875125 bytes  (spazio rimanente)
		 */
		assertEquals(124875, pit.getPitSize());
		System.out.println("Test passed!");
	}
	
	public void fullPIT() throws FullPITException, BadUriException {
		String[] fake_uri = new String[1];
		fake_uri[0] = "";
		Uri uri;
		boolean res;
		int i, j;
		/* La PIT viene riempita con 8001 uri da 125 bytes quindi:
		 * 8001 * 125 = 1000125 bytes
		 * Ma la PIT e' ampia 1000000 bytes -> FullPITException 
		 */
		for(i=0; i<8000; i++)
		{
			fake_uri[0] = "";
			/* Produco una uri lunga esattamente 125 caratteri */
			for(j=0; j<125; j++)
				fake_uri[0] = fake_uri[0].concat(String.valueOf(Math.abs((int)(Math.random()*10)-1)));
			uri = new Uri(fake_uri);
			uri.setId(i);
			uri.initArray(1500, 1500);
			res = pit.addEntry(uri, i, router.getInterfaceFromId("21"));
			Assert.assertEquals(true, res);
		}
		for(j=0; j<125; j++)
			fake_uri[0] = fake_uri[0].concat(String.valueOf(Math.abs((int)(Math.random()*10)-1)));
		uri = new Uri(fake_uri);
		uri.setId(i);
		uri.initArray(1500, 1500);
		res = pit.addEntry(uri, i, router.getInterfaceFromId("21"));
		/* Quando la PIT � piena addEntry() ritorna false (anzich� lanciare un exception) */
		Assert.assertEquals(false, res);
	}
	
	@Test
	public void PITload() throws BadUriException, FullPITException{
		String[] fake_uri = new String[1];
		Uri uri;
		fake_uri[0] = "";
		int i, j;
		for(i=0; i<500; i++)
		{
			fake_uri[0] = "";
			/* Produco una uri lunga esattamente 125 caratteri */
			for(j=0; j<1000; j++)
				fake_uri[0] = fake_uri[0].concat(String.valueOf(Math.abs((int)(Math.random()*10)-1)));
			uri = new Uri(fake_uri);
			uri.initArray(1500, 1500);
			uri.setId(i);
			pit.addEntry(uri, i, router.getInterfaceFromId("21"));
		}
		/* PIT = 1 MB */
		double load = pit.getLoad();
		Assert.assertEquals(0.5, load, 0.0001);
	}

}
