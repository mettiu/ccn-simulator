package it.polito.ccn.testing;

import static org.junit.Assert.*;
import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.Link;
import it.polito.ccn.simulator.exception.LinkBusyException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.junit.Test;
import org.xml.sax.SAXException;

public class TestLink {
	
	private CCNNetwork network;
	
	public TestLink() throws JAXBException, SAXException
	{
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "true");
	}

	@Test(expected=LinkBusyException.class)
	public void testLink1() throws LinkBusyException {
		Link l = network.getLinkById("l1");
		assertNotNull(l);
		l.takeLink("r1");
		l.takeLink("r1");
	}
	
	@Test(expected=LinkBusyException.class)
	public void testLink2() throws LinkBusyException {
		Link l = network.getLinkById("l1");
		assertNotNull(l);
		l.takeLink("r1");
		l.takeLink("r1");
	}
	
	@Test
	public void testLink3() throws LinkBusyException
	{
		Link l = network.getLinkById("l1");
		assertNotNull(l);
		l.takeLink("r1");
		l.freeLink("r1");
		l.takeLink("r1");
		l.freeLink("r1");
		l.takeLink("r1");
		l.takeLink("c1");
		l.freeLink("c1");
		l.freeLink("r1");
	}

}
