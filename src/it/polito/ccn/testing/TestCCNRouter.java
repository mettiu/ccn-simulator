package it.polito.ccn.testing;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.ccn.simulator.CCNContentObject;
import it.polito.ccn.simulator.CCNInterest;
import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.CCNNode;
import it.polito.ccn.simulator.CCNPacket;
import it.polito.ccn.simulator.CCNCoreRouter;
import it.polito.ccn.simulator.Event;
import it.polito.ccn.simulator.Routable;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import org.junit.Test;
import org.xml.sax.SAXException;

public class TestCCNRouter {
	
	private CCNNetwork network;
	private CCNCoreRouter router;
	
	public TestCCNRouter() throws JAXBException, SAXException
	{
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/testRouter.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "false");
		Iterator<CCNNode> i = network.getRouters().iterator();
		while(i.hasNext())
		{
			CCNNode r = i.next();
			if(r.getId().equals("r1"))
				router = (CCNCoreRouter)r;
		}
	}

	@Test
	public void testRouter1() throws BadUriException {
		assertEquals(0, router.getPIT().getElementsInPIT());
		String[] components = {"it", "polito", "example.avi", "s0"};
		Uri uri = new Uri(components);
		uri.setId(0);
		uri.initArray(10000, 1500);
		CCNPacket packet = new CCNInterest(uri, 0, 0, 20, 1500, null);
		Event event = new Event(0, Event.Type.NEW_INTEREST, router.getLinkFromId(network.getLinks(), "l1"), null, packet);
		/* Simulo l'arrivo di una nuova interest sul link l1 */
		List<Routable> routable = router.processPacket(event);
		assertEquals(1, routable.size());
		assertEquals("l2", routable.get(0).getLink().getId());
		/* Simulo l'arrivo di una nuova interest sul link l2 */
		event = new Event(0, Event.Type.NEW_INTEREST, router.getLinkFromId(network.getLinks(), "l2"), null, packet);
		routable = router.processPacket(event);
		assertNull(routable);
		assertEquals(1, router.getPIT().getElementsInPIT());
		packet = new CCNContentObject(uri, 0, 20, 1500, false, null);
		event = new Event(0, Event.Type.NEW_INTEREST, router.getLinkFromId(network.getLinks(), "l2"), null, packet);
		routable = router.processPacket(event);
		/* 
		 * Per l'url /it/polito/example.avi/s0 abbiamo le interfacce verso l1 ed l2 pending. Il pacchetto arriva da l2 quindi
		 * il router instradera' solo verso l1 perche' l'oggetto arriva da l2 e quindi anche se e' in lista non lo inoltra di nuovo.
		 */
		assertEquals(1, routable.size());
		assertEquals(0, router.getPIT().getElementsInPIT());
	}
	
	@Test
	public void testRouter2() throws BadUriException {
		/* Interest verso destinazione ignota */
		System.out.println("Ignore errors.");
		String[] components = {"hackers.net", "video", "prova", "example.avi", "s0"};
		Uri uri = new Uri(components);
		uri.setId(0);
		uri.initArray(10000, 1500);
		CCNPacket packet = new CCNInterest(uri, 0, 0, 20, 1500, null);
		Event event = new Event(0, Event.Type.NEW_INTEREST, router.getLinkFromId(network.getLinks(), "l1"), null, packet);
		List<Routable> routable = router.processPacket(event);
		assertEquals(0, router.getPIT().getElementsInPIT());
		assertNull(routable);
	}

}
