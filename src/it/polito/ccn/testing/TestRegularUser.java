package it.polito.ccn.testing;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.CCNRegularClient;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import org.xml.sax.SAXException;

public class TestRegularUser {
	
	private CCNNetwork network;
	private CCNRegularClient user;
	
	public TestRegularUser() throws SAXException, JAXBException
	{
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "true");
		user = network.getUsers().get(0);
		System.out.println(user.getId());
	}

}
