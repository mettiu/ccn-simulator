package it.polito.ccn.testing;

import it.polito.ccn.simulator.usersgen.POP;
import it.polito.ccn.simulator.usersgen.UsersAllocation;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class UsersAllocationCalc {
	
	private static final String file = "/resources/UsersAllocation.xml";
	private static final int totalUsers = 10000;
	
	public static void main(String[] args) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.usersgen");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		try {
			schema = sf.newSchema(u.getClass().getResource("/resources/UsersAllocationSchema.xsd"));
		} catch (SAXException e) {
			e.printStackTrace();
			System.out.println("Invalid schema.");
		}
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<UsersAllocation> j = (JAXBElement<UsersAllocation>)u.unmarshal(u.getClass().getResource(UsersAllocationCalc.file));
		UsersAllocation users = (UsersAllocation) j.getValue();
		Iterator<POP> i = users.getPOP().iterator();
		POP pop;
		DecimalFormat dF = new DecimalFormat("#.####");
		DecimalFormatSymbols d = DecimalFormatSymbols.getInstance();
		d.setDecimalSeparator('.');
		dF.setDecimalFormatSymbols(d);
		System.out.println("Valori non normalizzati");
		double norm = 0.0;
		while(i.hasNext())
		{
			pop = i.next();
			System.out.println(pop.getID() + " = " + pop.getValue());
			norm += pop.getValue();
		}
		i = users.getPOP().iterator();
		System.out.println("Total users = " + totalUsers);
		System.out.println("***Valori normalizzati:");
		double temp = 0.0;
		double t;
		while(i.hasNext())
		{
			pop = i.next();
			t = (double)(pop.getValue()/norm);
			temp += t;
			System.out.println(pop.getID() + " = " + dF.format(t) + ". Users supposed = " + (int)(t*totalUsers));
		}
		System.out.println("Integrale="+dF.format(temp));
	}

}
