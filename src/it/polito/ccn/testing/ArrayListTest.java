package it.polito.ccn.testing;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Assert;

import org.junit.Test;

public class ArrayListTest {
	
	private ArrayList<String> list;
	
	public ArrayListTest()
	{
		list = new ArrayList<String>();
	}
	
	@Test
	public void test1()
	{
		list.add(new String("1"));
		list.add(new String("2"));
		list.add(new String("3"));
		list.add(new String("4"));
		Iterator<String> i = list.iterator();
		System.out.println("ARRAY 1");
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		String s = list.remove(list.size()-1);
		Assert.assertEquals(0, s.compareTo("4"));
		Assert.assertEquals(3, list.size());
		i = list.iterator();
		System.out.println("ARRAY 2");
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		s = list.remove(0);
		Assert.assertEquals(0, s.compareTo("1"));
		Assert.assertEquals(2, list.size());
		i = list.iterator();
		System.out.println("ARRAY 3");
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
	}

}
