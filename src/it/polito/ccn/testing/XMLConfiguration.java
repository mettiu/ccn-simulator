package it.polito.ccn.testing;

import java.util.ArrayList;

import it.polito.ccn.simulator.CCNAttacker;
import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.Link;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;
import it.polito.ccn.simulator.generated.LinkG;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class XMLConfiguration {

	/**
	 * @param args
	 * @throws JAXBException 
	 * @throws BadUriException 
	 */
	public static void main(String[] args) throws JAXBException, BadUriException {
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		try {
			schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		} catch (SAXException e) {
			e.printStackTrace();
			System.out.println("Invalid configuration file.");
		}
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		/* Test delle funzionalit� di match degli Uri */
		try
		{
			String[] uri = new String[4];
			uri[0] = new String("polito.it");
			uri[1] = new String("video");
			uri[2] = new String("prova");
			uri[3] = new String("*");
			// URI = /polito.it/video/*
			Uri uri1 = new Uri(uri);
			String[] anotherUri = new String[4];
			anotherUri[0] = new String("polito.it");
			anotherUri[1] = new String("video");
			anotherUri[2] = new String("prova");
			anotherUri[3] = new String("example.avi");
			// URI2 = /polito.it/video/prova/example.avi
			Uri uri2 = new Uri(anotherUri);
			if(uri2.match(uri1))
				System.out.println(uri2 + " match " + uri1);
			else
				System.out.println("Not match!");
			System.out.println(uri2 + " = " + uri2.getUriSizeInByte() + " bytes");
		}
		catch(Exception ex) { System.out.println("Exception!"); }
		Runnable network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "true");
		Thread t = new Thread(network);
		t.start();
		System.out.println("****Rete avviata****");
		try {
			t.join();
			System.out.println("-=Simulazione terminata=-");
			LinkG linkG = new LinkG();
			linkG.setId("l1");
			linkG.setDown(100);
			linkG.setUp(100);
			linkG.setDelay(0);
			Link link = new Link(linkG);
			String[] components = new String[3];
			components[0] = "polito.it";
			components[1] = "video";
			components[2] = "movie.avi";
			Uri uri = new Uri(components);
			ArrayList<Link> aList = new ArrayList<Link>();
			aList.add(link);
			CCNAttacker attacker = new CCNAttacker((CCNNetwork)network, "attacker1", aList, uri, CCNAttacker.Type.LONG_URI_FLOOD, 30);
			double time = 0;
			int packetsAttacker = 0;
			while(time < 1)
			{
				packetsAttacker++;
				time+=(20 + uri.getUriSizeInByte())*8 / (attacker.getLink().getCapacity(attacker.getId()) * Math.pow(10, 6));
			}
			int dim = 20 + uri.getUriSizeInByte();
			System.out.println("Dimensione singolo pacchetto = " + dim + " byte");
			System.out.println("Generati " + packetsAttacker + " pacchetti dall'attaccante in " + time + " sec");
			//System.exit(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
