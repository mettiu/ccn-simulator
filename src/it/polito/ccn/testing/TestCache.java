package it.polito.ccn.testing;

import static org.junit.Assert.*;
import it.polito.ccn.simulator.CCNNetwork;
import it.polito.ccn.simulator.Cache;
import it.polito.ccn.simulator.Uri;
import it.polito.ccn.simulator.exception.BadUriException;
import it.polito.ccn.simulator.generated.CCNConfigurationG;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.junit.Test;
import org.xml.sax.SAXException;

public class TestCache {
	
	private Cache cache;
	
	public TestCache() throws JAXBException, SAXException
	{
		JAXBContext jc = JAXBContext.newInstance("it.polito.ccn.simulator.generated");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		schema = sf.newSchema(u.getClass().getResource("/resources/NetworkSchema.xsd"));
		u.setSchema(schema);
		@SuppressWarnings("unchecked")
		JAXBElement<CCNConfigurationG> j = (JAXBElement<CCNConfigurationG>)u.unmarshal(u.getClass().getResource("/resources/NetworkConfiguration.xml"));
		CCNConfigurationG ccnConf = (CCNConfigurationG) j.getValue();
		CCNNetwork network = new CCNNetwork(ccnConf, "7", "Core", "HashedPIT", "true");
		cache = new Cache(network, 1);
	}

	@Test
	public void testLRU1() throws BadUriException {
		String[] anotherUri = {"polito.it", "video", "prova", "example.avi", "s0"};
		Uri uri = new Uri(anotherUri);
		uri.setId(0);
		uri.initArray(10000, 1500);
		cache.addElement(uri, 0, 100000);
		String[] anotherUri2 = {"polito.it", "video", "prova", "ciao", "s0"};
		Uri uri2 = new Uri(anotherUri2);
		uri2.setId(1);
		uri2.initArray(10000, 1500);
		cache.addElement(uri2, 0, 10000);
		String[] anotherUri3 = {"polito.it", "video", "prova", "ciao", "s1"};
		Uri uri3 = new Uri(anotherUri3);
		uri3.setId(2);
		uri3.initArray(10000, 1500);
		cache.addElement(uri3, 0, 20000);
		assertEquals(2, cache.cacheSize());
		assertEquals(0, (int)cache.searchObject(uri, 0));
		assertEquals(10000, (int)cache.searchObject(uri2, 0));
		assertEquals(20000, (int)cache.searchObject(uri3, 0));
		System.out.println("******Test LRU 1/3 passed!******");
	}
	
	@Test
	public void testLRU2() throws BadUriException {
		String[] anotherUri = {"polito.it", "video", "prova", "example.avi", "s0"};
		Uri uri = new Uri(anotherUri);
		uri.setId(0);
		uri.initArray(10000, 1500);
		cache.addElement(uri, 0, 100000);
		
		String[] anotherUri2 = {"polito.it", "video", "prova", "ciao", "s0"};
		Uri uri2 = new Uri(anotherUri2);
		uri2.setId(1);
		uri2.initArray(10000, 1500);
		cache.addElement(uri2, 0, 10000);
		
		int result = cache.searchObject(uri, 0);
		System.err.println(result);
		
		String[] anotherUri3 = {"polito.it", "video", "prova", "ciao", "s1"};
		Uri uri3 = new Uri(anotherUri3);
		uri3.setId(2);
		uri3.initArray(10000, 1500);
		cache.addElement(uri3, 0, 20000);
		assertEquals(2, cache.cacheSize());
		assertEquals(100000, (int)cache.searchObject(uri, 0));
		/* anotherUri2 non c'e' in cache perche' e' stato rimpiazzato */
		assertEquals(0, (int)cache.searchObject(uri2, 0));
		assertEquals(20000, (int)cache.searchObject(uri3, 0));
		System.out.println("******Test LRU 2/3 passed!******");
	}
	
	@Test
	public void testLRU3() throws BadUriException {
		String[] anotherUri = {"polito.it", "video", "prova", "example.avi", "s0"};
		Uri uri = new Uri(anotherUri);
		uri.setId(0);
		uri.initArray(10000, 1500);
		cache.addElement(uri, 0, 10000);
		
		String[] anotherUri2 = {"polito.it", "video", "prova", "ciao", "s0"};
		Uri uri2 = new Uri(anotherUri2);
		uri2.setId(1);
		uri2.initArray(10000, 1500);
		cache.addElement(uri2, 0, 20000);
		
		String[] anotherUri3 = {"polito.it", "video", "prova", "ciao", "s1"};
		Uri uri3 = new Uri(anotherUri3);
		uri3.setId(2);
		uri3.initArray(10000, 1500);
		cache.addElement(uri3, 0, 120000);
		assertEquals(1, cache.cacheSize());
		assertEquals(0, (int)cache.searchObject(uri, 0));
		assertEquals(0, (int)cache.searchObject(uri2, 0));
		assertEquals(120000, (int)cache.searchObject(uri3, 0));
		System.out.println("******Test LRU 3/3 passed!******");
	}

}
