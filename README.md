**Prerequisites**
- Java 1.7+ (tested on 1.8.0_91)

**How to run an example**
- Import the project in Eclipse
- Create a running config for the RunningTest class
- Set "/resources/testLifetime4s.xml" as program arguments (this is the topology file which is compliant with NetworkSchema.xsd, inspect it for further details)
- Set "-DattackerUriDim=1000 -DrouterType=Core -DpitType=SimplePIT -DenableGui=true" as VM arguments (routerType can be Core or DiPIT, pitType can be DiPIT, DiPITBF, SimplePIT, HashedPIT. See CCNComponentFactory.java for further details)
- Run it!

**PIT implementation**
The simulator contains different implementations for the Pending Interest Table:
- SimplePIT.java, a PIT that stores the whole content name;
- HashedPIT.java, a PIT that stores an hash of the content name;
- DiPIT, a distributed PIT which has been introduced in http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=6289282
DiPIT is available in two versions: DiPIT.java implemented with the mathematical model of the Bloom filter and DiPITBF.java implemented with an actual Bloom Filter.
All PITs inherits from PIT.java which is an abstract class that can be extended to create custom PIT implementations.