blue = "#0000FF"
set samples 32
set terminal win
set yrange [0:0.1]
set xrange [0:33]
set xtic rotate by -45 scale 0
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set key right top Left
set grid
set xlabel "POP"
set ylabel "Fractions of users over the total"

#set title "Fractions of users on different POPs"
set ytics ("0.01" 0.01, "0.02" 0.02, "0.03" 0.03, "0.04" 0.04, "0.05" 0.05, "0.06" 0.06, "0.07" 0.07, "0.08" 0.08, "0.09" 0.09, "0.1" 0.1)
set xtics add ("MI1" 1, "MI2" 2, "RM1" 3, "RM2" 4, "NA" 5, "TO" 6, "NL" 7, "CT" 8, "FI" 9, "PA" 10, "PI" 11, "BA" 12, "MO" 13, "AN" 14, "BO" 15, "VE" 16, "CZ" 17, "BS" 18, "TA" 19, "VR" 20, "PD" 21, "RN" 22, "BG" 23, "PE" 24, "TS" 25, "AL" 26, "BZ" 27, "GE" 28, "CA" 29, "CO" 30, "PG" 31, "SV" 32)
plot 'datafile' u 3 lc rgb blue title "Fractions of users on different POPs"
#, title 'Fractions of users on different POPs' with boxes
pause -1