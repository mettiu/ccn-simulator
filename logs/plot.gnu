blue = "#0000FF"
#set samples 32
set terminal win
#set xtic rotate by -45 scale 0
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set key left top Left
set grid
set xlabel "Tempo [sec]"
set ylabel "Numero di entries - POP di Roma"
set y2label "Occupazione memoria (in MB) - POP di Roma"
set y2tics nomirror
set ytics nomirror
set format y "%s"
set xrange [0:290]
#set title "Utenti attivi"
#set ytics ("0.01" 0.01, "0.02" 0.02, "0.03" 0.03, "0.04" 0.04, "0.05" 0.05, "0.06" 0.06, "0.07" 0.07, "0.08" 0.08, "0.09" 0.09, "0.1" 0.1)
#set xtics add ("MI1" 1, "MI2" 2, "RM1" 3, "RM2" 4, "NA" 5, "TO" 6, "NL" 7, "CT" 8, "FI" 9, "PA" 10, "PI" 11, "BA" 12, "MO" 13, "AN" 14, "BO" 15, "VE" 16, "CZ" 17, "BS" 18, "TA" 19, "VR" 20, "PD" 21, "RN" 22, "BG" 23, "PE" 24, "TS" 25, "AL" 26, "BZ" 27, "GE" 28, "CA" 29, "CO" 30, "PG" 31, "SV" 32)
set y2tics ("100 MB" 100, "200 MB" 200, "300 MB" 300, "400 MB" 400, "500 MB" 500, "600 MB" 600, "700 MB" 700, "800 MB" 800, "900 MB" 900, "1 GB" 1000)
plot 'routerrm100.dat' using 2:3 title "Entries nella PIT" with lines, 'routerrm100.dat' using 2:4 title "Occupazione memoria (in MB)" with lines axes x1y2
#, title 'Frazione di utenti attestati al POP' with boxes
pause -1